/** Interface Baum mit 4 Methoden **/

public interface Baum {
	public boolean empty();
	public Object value();
	public Baum rechts();
	public Baum links();
}
