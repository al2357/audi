import AlgoTools.io;

public class Traverse {
	public static void inorder(Baum b) {
		if(!b.empty()) {
			inorder(b.left());
			IO.print(b.value());
			inorder(b.right());
		}
	}
	
	public static void preorder(Baum b) {
		if(!b.empty()) {
			IO.print(b.value());
			preorder(b.left());
			preorder(b.right());
		}
	}
	
	public static void postorder(Baum b) {
		if(!b.empty()) {
			postorder(b.left());
			postorder(b.right());
			IO.print(b.value());
		}
	}
	
	public static void klammerinorder(Baum b) {
		if(!b.empty()) {
			if(!b.left().empty()) { IO.print("("); }
			klammerinorder(b.left());
			IO.print(b.value());
			klammerinorder(b.right());
			if(!b.right().empty()) { IO.print(")"); }
		}
	}
}
