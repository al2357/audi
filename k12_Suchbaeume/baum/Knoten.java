/** Klasse Knoten mit einem Konstruktor **/

public class Knoten {
	Object inhalt;
	Knoten links, rechts;
	
	public Knoten(Object x) {
		this.inhalt = x;
		rechts = links = null;
	}
}
