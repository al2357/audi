/** VerweisBaum class **/

public class VerweisBaum implements Baum {
	Knoten wurzel;
	
	public VerweisBaum() {
		wurzel = null;
	}
	
	public VerweisBaum(Object x) {
		wurzel = new Knoten(x);
	}
	
	public VerweisBaum(Knoten k) {
		wurzel = k;
	}
	
	public VerweisBaum(VerweisBaum l, Object x, VerweisBaum r) {
		wurzel = new Knoten(x);
		if(l != null) { wurzel.links = l };
		if(r != null) { wurzel.rechts = r };
	}
	
	public boolean empty() {
		return wurzel == null;
	}
	
	public Object value() {
		if(empty()) { throw new RuntimeException(" Baum ist leer "); }
		return wurzel.inhalt;
	}
	
	// Kann Baum sein(statt VerweisBaum) weis Baum ein Superklasse ist.
	public Baum links() 
		if(empty()) { throw new RuntimeException(" Baum ist leer "); }
		return new VerweisBaum(wurzel.links);
	}
	
	public Baum rechts() {
		if(empty()) { throw new RuntimeException(" Baum ist leer "); }
		return new VerweisBaum(wurzel.rechts);
	}
	
}
