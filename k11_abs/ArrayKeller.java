/***************************  ArrayKeller.java  ******************************/

/**
 * Implementiert das Interface <tt>Keller</tt> mittels eines Arrays.
 */
public class ArrayKeller implements Keller {

  private Object[] keller;
  private int Spitze;

  /**
   * Erzeugt einen ArrayKeller.
   *
   * @param anfangsGroesse
   *          Initialgroesse der internen Datenstruktur
   */
  public ArrayKeller(int anfangsGroesse) {
    keller = new Object[anfangsGroesse];
    Spitze = -1;
  }

  /**
   * Erzeugt einen ArrayKeller mit initialgroesse 8.
   */
  public ArrayKeller(){
    this(8);
  }
  
  /**
  * Erweitert das Feld dynamisch, dynamisches Kellerwachstum.
  */
  private void erweitern(int groesse) {
  	Object[] neue_feld = new Object[groesse];
  	System.arraycopy(keller, 0, neue_feld, 0, keller.length);
  	keller = neue_feld;
  }

  /**
   * Prueft, ob der Keller leer ist.
   *
   * @return true bei leerem Keller, false sonst
   */
  public boolean empty() {
    return (Spitze == -1);
  }

  /**
   * Legt das uebergebene Element auf den Keller.
   *
   * @param x
   *    Das hinzuzufuegende Element
   */
  public void push(Object x) {
  if(Spitze + 1 >= keller.length) erweitern(Spitze + 2);
    keller[++Spitze] = x;
  }

  /**
   * Liefert das aktuelle Top-Element des Kellers
   *
   * @throws RuntimeException bei leerem Keller
   * @return Top-Element
   */
  public Object top() {
    if(empty()) { throw new RuntimeException("Der Keller ist leer!"); }
    return keller[Spitze];
  }

  /**
   * Entfernt das aktuelle Top-Element vom Keller
   *
   * @throws RuntimeException bei leerem Keller
   */
  public void pop() {
  if(empty()) { throw new RuntimeException("Der Keller ist leer!"); }
    Spitze--;
  }
}

