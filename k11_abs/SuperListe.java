

/****************************** SuperListe.java ******************************/

/**
* Erweitet die Klasse VerweisListe um zwei nuetzliche Operationen.
*/

public class SuperListe extends VerweisListe {

/**
* Invertiert die Reihenfolge der Elemente in der Liste.
*/
public void umdrehen() {
Object zwischen = new Object(); // Zwischenspeicher
int n = listenElemente(); // Anzahl der Listenelemente ermitteln.
while(--n >= 0) { // Beginnend beim vorletzten Element, Elemente zwischenspeichern,
// loeschen und ans Ende haengen.
zwischen = elem(n); // Element von Position n zwischenspeichern.
delete(); // Element an Position n loeschen.
endeSetzen(); // An das Listenende springen.
insert(zwischen); // Zwischengespeichertes Element ans Ende haengen.
}

}

/**
* Springt auf das letzte Element der Liste.
*/
private void endeSetzen() {
while(!endpos())
advance();
}

/**
 * Zahl der Listenelemente ermitteln. Zaehlung beginnt bei 0.
 */

private int listenElemente() {
if(empty())
throw new RuntimeException("Liste ist leer");
int n = 0;
reset();
while(!endpos()) {
advance();
n++;
}
return n;
}

/**
* Liefert das Element an Stelle n.
*
* @param n der Index des zu liefernden Elementes, beginnend bei 0
*
* @return das Element an Stelle n
*
* @throws RuntimeException falls Liste weniger als n Elemente hat oder n < 0 ist
*/
public Object elem(int n){
if(n < 0 || listenElemente() < n)
throw new RuntimeException("Ungueltiger Index");
reset(); // Auf Listenanfang springen.
for(int i = 0; i < n; i++) // An Auslesestelle springen.
advance();
return super.elem(); // Element zurueckgeben.
}
}

