/******************************  SuperListeTest.java  **************************/

import AlgoTools.IO;

/**
 * Testet Methoden der Klasse SuperListe.
 */

public class SuperListeTest {

  public static void main (String [] argv) {

    SuperListe l = new SuperListe();

    l.insert("a");
    l.advance();
    l.insert("b");
    l.advance();
    l.insert("c");

    l.reset();

    l.umdrehen();

    IO.println("Listeninhalt der Testliste (Strings): a, b, c");
    IO.print("Umgedrehte Reihenfolge (sollte 'c b a' sein): ");

    l.reset();
    while(!l.endpos()){
      IO.print(l.elem() + " ");
      l.advance();
    }
    IO.println();

    IO.println();
    IO.println();

    IO.print("Rufe elem(1) auf. Ergebnis sollte der String  \"b\" sein: ");
    IO.println(l.elem(1));

  }
}

