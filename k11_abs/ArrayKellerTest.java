/*************************** ArrayKellerTest.java **************************/

import AlgoTools.IO;

/**
 * Test der Klasse SchlangenKeller.
 */
public class ArrayKellerTest {

  /**
   * Testet den ArrayKeller
   */
  public static void main(String[] args) {

    //Neuen ArrayKeller erstellen
    Keller sk = new ArrayKeller(10);

    //empty
    IO.print("Leerer Keller ist leer: ");
    if(sk.empty()) {
      IO.println("ERFOLGREICH");
    }
    else {
      IO.println("FEHLGESCHLAGEN");
    }

    //pop
    try {
      IO.print("pop() wirft RuntimeException, wenn Keller leer: ");
      sk.pop();
      IO.println("FEHLGESCHLAGEN");
    }
    catch(RuntimeException e) {
      IO.println("ERFOLGREICH");
    }

    //top
     try {
        IO.print("top() wirft RuntimeException, wenn Keller leer: ");
        sk.top();
        IO.println("FEHLGESCHLAGEN");
      }
      catch(RuntimeException e) {
        IO.println("ERFOLGREICH");
      }

    //empty
    IO.print("Nicht leerer Keller ist nicht leer: ");
    sk.push(1);
    sk.push(2);
    sk.push(3);

    if(sk.empty()) {
      IO.println("FEHLGESCHLAGEN");
    }
    else {
      IO.println("ERFOLGREICH");
    }

    //top, pop und push
    IO.print("top(), pop() und push() arbeitet korrekt: ");
    if((Integer) sk.top() == 3) {
      sk.pop();
      if((Integer) sk.top() == 2) {
        sk.pop();
        if((Integer) sk.top() == 1) {
          IO.println("ERFOLGREICH");
        }
        else {
          IO.println("FEHLGESCHLAGEN");
        }
      }
      else {
        IO.println("FEHLGESCHLAGEN");
      }
    }
    else {
      IO.println("FEHLGESCHLAGEN");
    }

    // dynamisches Wachsen
    IO.print("Dynamisches Kellerwachstum: ");
    sk = new ArrayKeller(1);
    sk.push(42);
    sk.push(23);
    sk.push(73);

    if((Integer) sk.top() == 73){
      sk.pop();

      if((Integer) sk.top() == 23){
        sk.pop();

        if((Integer) sk.top() == 42){
          IO.println("ERFOLGREICH");
        } else {
          IO.println("FEHLGESCHLAGEN");
        }
      } else {
        IO.println("FEHLGESCHLAGEN");
      }

    }else {
      IO.println("FEHLGESCHLAGEN");
    }

  }
}
