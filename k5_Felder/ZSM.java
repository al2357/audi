import AlgoTools.IO;

/**
 * Liest eine natürliche Zahl n ein, liest dann eine quadratische Matrix mit
 * Seitenlaenge n ein, berechnet deren Zeilensummennorm und gibt diese aus.
 */
public class ZSM {
  public static void main(String[] args) {

    int n;

    // Seitenlaenge der quadratischen Matrix einlesen
    do{
      n = IO.readInt("Bitte die Groesse der Matrix: ");
    } while(n < 1);

    int[][] matrix = new int[n][n];

    // Fuer jede Zeile ein Iterationsschritt
    for(int i = 0; i < n; i++){
      int[] ary ;

      // Die Zeile in einem Schritt mit der passenden Laenge einlesen
      do{
        ary = IO.readInts("Bitte Zeile " + (i+1) + " der Matrix:");
      } while(ary.length != n);

      matrix[i] = ary;
    }

    // Eingelesene Matrix ausgeben
    IO.println("Die eingegebene Matrix lautete:");
    for(int i = 0; i < n; i++){
      for(int j = 0; j < n; j++){
        IO.print(matrix[i][j], 3);
      }
      IO.println();
    }

    // ENDE VORGEGEBENER CODE, es ist garantiert, dass in matrix jetzt
    // eine quadratische Matrix mit Seitenlaenge n enthalten ist

    int maxNorm = 0;
    int tmpSumme;
    int tmpInt;

    // iteriere ueber alle Zeilen
    for(int i = 0; i < n; i++){
      // resette tmpSumme, da evtl. gesetzt von voriger Zeile
      tmpSumme = 0;

      // summiere alle Elemente von Zeile i in tmpSumme auf
      for(int j = 0; j < n; j++){
        // Betrag bilden um negative Zahlen positiv zu machen
        tmpInt = matrix[i][j];
        tmpInt = tmpInt < 0 ? -1 * tmpInt : tmpInt;

        tmpSumme += tmpInt;
      }

      if(tmpSumme > maxNorm){
        maxNorm = tmpSumme;
      }

    }

    IO.println("Zeilensummennorm ist: " + maxNorm);

  }
}
