

import AlgoTools.IO;

public class Dodont {

public static void main(String[] args) {

 int n;

    // Anzahl der Zellen
    do{
      n = IO.readInt("Bitte geben Sie die Anzahl der Zellen an: ");
    } while(n < 0);

    //Zellen initialisieren, zunächst verschlossen
    boolean[] zelle = new boolean[n];
    for(int i = 0; i <= n-1; i++){
      zelle[i] = false;
    }   
int z=0;
    for(int bote = 1; bote <= n;  bote ++){
        for (int zellennummer = bote ; zellennummer <= n; zellennummer++){
            //WEnn die Zellennummer ein vielfaches der Botennummer ist..
            if(zellennummer % bote == 0){
                //einmal umdrehen - Wert ändern, je nach aktuellem wert öffnen oder verschliessen
                if(zelle[zellennummer-1]){
                    zelle[zellennummer-1] = false;
                }else{
                    zelle[zellennummer-1] = true;
                }
               
            } 
            z++;
        }

    }
    //Ausgabe der unverschlossenen Türen

    for(int i = 1 ; i <= n ; i++){
        if(zelle[i-1]){     
            IO.println(i);
        }
    }
    
    IO.println("Schleifen: "+z);

}

}

