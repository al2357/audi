import AlgoTools.IO;

/**
 *   Loest das Problem der Gefangenen des Koenigs Dodon mit variabler
 *   Anzahl an Zellen.
 *   Am Ende bleiben die Zellen mit Quadratzahlen an den Tueren offen, denn
 *   nur diese habe eine ungerade Anzahl verschiedener Teiler.
 */

public class Dodon {

  public static void main (String argv[]) {

    // Anzahl der Zellen
    int n;

    n = IO.readInt("Wieviele Zellen moechten Sie anlegen? ");

    boolean[] zellen = new boolean[n];            // Array mit Zellentueren.
                                                  // false wenn zu; true sonst

    IO.println("Folgende Zellen sind offen:");

    for(int bote = 1; bote <= n; bote++) {            // Fuer jeden Boten
      for(int tuer = bote-1 ; tuer < n; tuer += bote) {// drehe Schluessel im jedem
        zellen[tuer] = !zellen[tuer];                 // k-ten Schloss ab dem k-ten
      }
      if(zellen[bote-1]) {                            // k-ter Bote schliesst k-te
        IO.println(bote, 5);                          // Tuer als letzter um
      }
    }
  }
}

