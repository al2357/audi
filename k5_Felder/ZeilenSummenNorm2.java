import AlgoTools.IO;

/**
 * Liest eine natürliche Zahl n ein, liest dann eine quadratische Matrix mit
 * Seitenlaenge n ein, berechnet deren Zeilensummennorm und gibt diese aus.
 */
public class ZeilenSummenNorm2 {
  public static void main(String[] args) {

    int n;

    // Seitenlaenge der quadratischen Matrix einlesen
    do{
      n = IO.readInt("Bitte die Groesse der Matrix: ");
    } while(n < 1);

    int[][] matrix = new int[n][n];

    // Fuer jede Zeile ein Iterationsschritt
    for(int i = 0; i < n; i++){
      int[] ary = new int[n];
      int j = 0;

      // Die Zeile in einem Schritt mit der passenden Laenge einlesen
      do{
        ary[j] = IO.readInt("Bitte Spalte " + (j+1) + ", Zeile " + (i+1) + " der Matrix:");
        j++;
      } while(j != n);

      matrix[i] = ary;
    }

    // Eingelesene Matrix ausgeben
    IO.println("Die eingegebene Matrix lautete:");
    for(int i = 0; i < n; i++){
      for(int j = 0; j < n; j++){
        IO.print(matrix[i][j], 3);
      }
      IO.println();
      IO.println();
    };

    // ENDE VORGEGEBENER CODE
    // Es ist hier garantiert, dass in matrix jetzt
    // eine quadratische Matrix mit Seitenlaenge n enthalten ist.


    // HIER MUSS IHRE LÖSUNG HIN
    // Create an array that contains sums of n rows
	int[] zsn = new int[n];
	int zsnMax = 0;
	
	// Berechnen die Zeilensummennorm der reellen (i x j)-Matrix
	for(int i=0; i < n; i++) {	
		zsn[i] = 0;
		for(int j=0; j<n; j++) {
			// Convert negative numbers to positive
			matrix[i][j] = (matrix[i][j] < 0) ? -matrix[i][j] : matrix[i][j];
			// Add up all numbers in a row
			zsn[i] = zsn[i] + matrix[i][j];
		};
		zsnMax = zsn[i] > zsnMax ? zsn[i] : zsnMax;
	};
	
	IO.print("Ergebnis: " + zsnMax);
	IO.println();
	
// *sorry for mixed, English/German comments but German is my 3-rd language
  }
}
