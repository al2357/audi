import AlgoTools.IO;

public class Kapitel5 {
	public static void main(String[] args) {
		BinareSuche();
	}
	
	public static void CharTest () {
		char zeile[];
		char c;
	
		int i, wert, ziffernwert;
		
		zeile = IO.readChars("Bitte ziffernfolge: ");
		
		wert = 0;
		
		for(i=0; i < zeile.length; i++) {
			c = zeile[i];
			ziffernwert = (int) c - (int) '0';
			wert = 10*wert + ziffernwert;
		}
		
		IO.println("Als int: "+wert);
	}
	
	public static void Primzahlen() {
		boolean[] prim;
		int n, i, j;
		
		n = IO.readInt("Bitte Primzahlgrenze: ");
		prim = new boolean[n];
		
		for (i=2; i<n; i++) {
			prim[i] = true;
		}
		
		for(i=2; i<n; i++) {
			if(prim[i]) {
				IO.println(i);
				for(j=i+i; j<n; j=j+i) {
					prim[j] = false;
				}
			}
		}
	}
	
	public static void BinareSuche() {
		int[] a = IO.readInts("Geben Sie bitte Zahlen: ");
		int b = IO.readInt("Eine Zahl bitte: ");
		
		int poczatek, srodek, koniec;
		poczatek = 0;
		koniec = a.length - 1;
		srodek = a.length/2;
		
		while(poczatek <= koniec && b != a[srodek]) {
			if(b<a[srodek]) {
				koniec = srodek-1;
				srodek = koniec / 2;	
			} else if (b > a[srodek]) {
				poczatek = srodek + 1;
				srodek = (srodek / 1) + poczatek;
			}
		}
		
		if(b != a[srodek]) {
			IO.println("Die Zahl nicht gefunden. ");
		} else {
			IO.println("Die Zahl gefunden an der Position " + srodek);
		}
		
		}
			
}
