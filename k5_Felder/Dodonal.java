import AlgoTools.IO;

public class Dodonal {
	public static void main(String[] args) {
		int n = 0;
		do {
			n = IO.readInt("Eine Dodon-Zahl bitte: ");
		} while (n <= 0);
		
		boolean[] zelle = new boolean[n];
		int zahler = 0;		
		
		for(int i = 1; i <= n; i++) {
			for(int j=i; j <= n; j = j + i) {
				zelle[j-1] = !zelle[j-1];
			}
			if(zelle[i-1]) {
				IO.println(i);
				zahler++;
			}
		}
		
		IO.println("Ergebnis: "+zahler);
		
	}
}
