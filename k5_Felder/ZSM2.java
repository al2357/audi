import AlgoTools.IO;

/**
 * Liest eine natürliche Zahl n ein, liest dann eine quadratische Matrix mit
 * Seitenlaenge n ein, berechnet deren Zeilensummennorm und gibt diese aus.
 */
public class ZSM2 {
  public static void main(String[] args) {

    int n;

    // Seitenlaenge der quadratischen Matrix einlesen
    do{
      n = IO.readInt("Bitte die Groesse der Matrix: ");
    } while(n < 1);

    int[][] matrix = new int[n][n];

    // Fuer jede Zeile ein Iterationsschritt
    for(int i = 0; i < n; i++){
      int[] ary = new int[n];
      int j = 0;

      // Die Zeile in einem Schritt mit der passenden Laenge einlesen
      do{
        ary[j] = IO.readInt("Bitte Spalte " + (j+1) + ", Zeile " + (i+1) + " der Matrix:");
        j++;
      } while(j != n);

      matrix[i] = ary;
    }

    // Eingelesene Matrix ausgeben
    IO.println("Die eingegebene Matrix lautete:");
    for(int i = 0; i < n; i++){
      for(int j = 0; j < n; j++){
        IO.print(matrix[i][j], 3);
      }
      IO.println();
      IO.println();
    };

    // ENDE VORGEGEBENER CODE
    // Es ist hier garantiert, dass in matrix jetzt
    // eine quadratische Matrix mit Seitenlaenge n enthalten ist.


    // HIER MUSS IHRE LÖSUNG HIN

    // Berechnung der Zeilensummennorm
    // -------------------------------

    // 1. Umwandeln aller Zahlen in Ihre absolute Beträge

    for(int i = 0; i < n; i++){
      for(int j = 0; j < n; j++){
          if (matrix[i][j] < 0) {
              matrix [i][j] = -(matrix[i][j]);
          }
      }
    }

    // 2. Ermitteln der einzelnen Zeilensummen

    int summe = 0;
    int zaehler = 0;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            if ((i+j) < n){
                summe += matrix [i][j];     
            }
            if (summe > zaehler){
                zaehler = summe;
            }
        }
    } 
    IO.println("Die Zeilensummennorm beträgt: " + zaehler);

  }
}
