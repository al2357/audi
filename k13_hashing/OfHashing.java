import AlgoTools.IO;

public class OfHashing implements Menge {
	private Liste[] b;
	private int count;
	
	public OfHashing(int N) {
		b = new Liste[N];
		for (int i=0; i<N; i++) {
			b[i] = new VerweisListe();
		}
		count = 0;
	}
	
	private int hash(Comparable x) {
		int i, summe = 0;
		String s = x.toString();
		for(i=0; i<s.length(); i++) {
			// charAt returns char? if so why it's added as int?
			summe += s.charAt(i);
		}
		return summe % b.length;
	}
	
	public boolean empty() {
		return count == 0;
	}
	
	private boolean find(Liste l, Comparable x) {
		l.reset();
		while(!l.endpos() && ((Comparable)l.elem().compareTo(x) != 0);
			l.advance();
		return !l.endpos();
	}
	
	public Comparable lookup(Comparable x) {
		 int i = hash(x);
		 if(find(b[i], x))
			return (Comparable)b[i].elem();
		else return null;
	}
	
	public boolean insert(Comparable x) {
		int i = hash(x);
		if(!find(b[i], x)) {
			b[i].insert(x);
			count++;
			return true;
		} else return false;
	}
	public boolean delete(Comparable x) {
		int i = hash(x);
		if(find(b[i], x)) {
			b[i].delete();
			count--;
			return true;
		} else return false;
	}
}