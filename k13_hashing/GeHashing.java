import AlgoTools.IO;

public class GeHashing implements Menge {
	private Comparable[] inhalt;
	private byte[] zustand;
	private int count;
	
	public GeHashing(int N) {
		inhalt = new Comparable[N];
		zustand = new byte[N];
		for(int i=0; i<N; i++) {
			zustand[i] = 0;
		}
		count = 0;
	}
	
	private int hash(Comparable x) {
		int i, summe = 0;
		String s = x.toString();
		for(i=0; i<s.length(); i++) {
			// charAt returns char? if so why it's added as int?
			summe += s.charAt(i);
		}
		return summe % b.length;
	}
	
	public boolean empty() {
		return count == 0;
	}
	
	public Comparable lookup(Comparable x) {
		 int i = hash(x);
		 if(find(b[i], x))
			return (Comparable)b[i].elem();
		else return null;
	}
	
	public boolean insert(Comparable x) {
		int i = hash(x);
		if(!find(b[i], x)) {
			b[i].insert(x);
			count++;
			return true;
		} else return false;
	}
	
	public boolean delete(Comparable x) {
		int i = hash(x);
		if(find(b[i], x)) {
			b[i].delete();
			count--;
			return true;
		} else return false;
	}
}