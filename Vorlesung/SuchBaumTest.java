/*****************************  SuchBaumTest.java  ****************************/
import AlgoTools.IO;

/** Testet den SuchBaum mit Character-Objekten.  */

public class SuchBaumTest {

  public static void main(String[] argv) {
    
    SuchBaum s = new SuchBaum();

                                         // Elemente in SuchBaum einfuegen
    char[] eingabe = IO.readChars("Bitte Zeichen fuer insert: ");

    Comparable x = new Character('?');
    s.insert(x);

    for (int i = 0; i < eingabe.length; i++) {
      
      if (s.insert(new Character(eingabe[i])))
        IO.println(eingabe[i] + " eingefuegt");
      else
        IO.println(eingabe[i] + " konnte nicht eingefuegt werden");
    }

    IO.print("Inorder: ");               // Ausgabe: Inorder-Traversierung
    Traverse.inorder(s); 
    IO.println();
                                         // Elemente im SuchBaum suchen
    eingabe = IO.readChars("Bitte Zeichen fuer lookup: ");

    for (int i = 0; i < eingabe.length; i++) {

      Character c = (Character) s.lookup(new Character(eingabe[i]));
      
      if (c == null)
        IO.println(eingabe[i] + " konnte nicht gefunden werden");
      else
        IO.println(c + " gefunden");
    }
    
                                         // Elemente aus dem SuchBaum loeschen
    eingabe = IO.readChars("Bitte Zeichen fuer delete: ");

    for (int i = 0; i < eingabe.length; i++) {

      if (s.delete(new Character(eingabe[i])))
        IO.println(eingabe[i] + " geloescht");
      else
        IO.println(eingabe[i] + " konnte nicht geloescht werden");

      IO.print("Inorder: ");               // Ausgabe: Inorder-Traversierung
      Traverse.inorder(s);
      IO.println();
    }
  }
}
