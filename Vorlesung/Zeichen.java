/**************************  Zeichen.java  ************************************/

import AlgoTools.IO;

/**  Umwandlung von Character zur Zahl
 *   Umwandlung von Zahl zum Character
 */
 
public class Zeichen {

  public static void main (String [] argv) {

    for (int i=0; i<=255; i++) {                // fuer den Latin-1-Zeichensatz 
        IO.print(i,7);                          // drucke laufende Nummer
        IO.print( " " + (char) i );             // drucke zugehoeriges Zeichen
        if (i % 8 == 0) IO.println();           // nach 8 Zeichen neue Zeile 
    }
    IO.println();

    char c;                                     
    do {
        c = IO.readChar("Bitte ein Zeichen: "); // lies ein Zeichen ein
        IO.print("Der ASCII-Code lautet : ");   // gib den zugehoerigen
        IO.println((int) c,3);                  // ASCII-Code aus
    } while (c != '\n' );                       // bis ein Newline kommt
  }
}
