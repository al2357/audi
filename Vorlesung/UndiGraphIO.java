/***********************  UndiGraphIO.java ************************************/

import java.lang.*;
import java.util.*;
import java.io.*;
import AlgoTools.IO;

/** Routinen zum Einlesen und Ausgeben eines ungerichteten Graphen            */
/*  Der Graph wird realisiert durch eine HashMap,                             */
/*  welche den Namen des Knoten auf den Knoten abbildet.                      */
/*  Es wird erwartet, dass jede ungerichtete Kante nur einmal gelesen wird    */

public class UndiGraphIO {

  public static UndiGraph readUndiGraph() { // liest unger.Graph aus Datei ein
    UndiGraph g = new UndiGraph();
    try {
      BufferedReader f = new BufferedReader(new FileReader("undigraph.dat"));
      String zeile;
      while ( (zeile = f.readLine()) != null) {
        StringTokenizer st = new StringTokenizer(zeile);
        String source = st.nextToken();
        String dest   = st.nextToken();
        double cost   = Double.parseDouble(st.nextToken());
        g.addEdge(source,dest,cost);
      }
    } catch (Exception e) {IO.println(e);}
    return g;
  }

  public static void printUndiGraph(UndiGraph g) { // gibt Graph aus
    IO.println("Adjazenzlisten des Graphen:\n");
    for (UndiVertex v : g.vertices()) {
      for (UndiEdge e : v.edges) {
        if (e.left==v)
        IO.print("(" + e.left.name + "," + e.right.name + ")");
        else
        IO.print("(" + e.right.name + "," + e.left.name + ")");
        IO.print(e.cost + "  ");
      }
      IO.println(); 
    }
    IO.println();
  }
}
