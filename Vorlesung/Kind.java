/***************************  Kind.java  **************************************/
/** Klasse Kind 
 *  bestehend aus Nummer und Verweis auf Nachbarkind 
 *  mit Konstruktor zum Anlegen eines Kindes                                  */

public class Kind {
  int  nr;                                    // Nummer 
  Kind next;                                  // Verweis auf naechstes Kind 
  public Kind (int nr) {                      // Konstruktor fuer Kind
    this.nr   = nr;                           // initialisiere Nummer 
  }
}
