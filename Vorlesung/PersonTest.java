/***************************  PersonTest.java *********************************/
import AlgoTools.IO;

/** Klasse PersonTest, testet Klasse Person                                   */

public class PersonTest {

  public static void main (String [] argv) {
    Person p;                                     // deklariere eine Person
    p = new Person("Willi","Wacker",22,8,1972);   // kreiere Person
    p.geb_datum.jahr++;                           // mache sie 1 Jahr juenger 
    IO.print(p);                                  // gib Vor- und Nachname aus
    IO.println(" ist "+p.alter()+" Jahre alt.");  // gib Alter aus
  }
}
