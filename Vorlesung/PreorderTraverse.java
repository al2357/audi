/************************* PreorderTraverse.java ******************************/

import java.util.Enumeration;

/** Schrittweise Preordertraversierung eines Baumes mit Interface Enumeration 
 */

public class PreorderTraverse implements Enumeration {

  private Keller k;                            // Keller zum Zwischenspeichern
                       
  public PreorderTraverse(Baum wurzel) {       // Konstruktor,
    k = new VerweisKeller();                   // besorge Verweiskeller 
    if (!wurzel.empty()) k.push(wurzel);       // initialisiert Keller
  }

  public boolean hasMoreElements() {           // liefert true
    return !k.empty();                         // falls noch Elemente
  }                                            // im Keller sind

  public Object nextElement() {             
    if (k.empty()) throw new RuntimeException("in nextElement: Keller leer");
    Baum b = (Baum)k.top();                    // hole Baum vom Keller
    k.pop();                                   // entferne Top-Element
    if (!b.right().empty()) k.push(b.right()); // lege rechten Sohn auf Keller
    if (!b.left().empty())  k.push(b.left());  // lege linken Sohn auf Keller
    return b.value();                          // liefere Element ab
  }
}
