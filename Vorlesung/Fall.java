/****************************  Fall.java  *************************************/

import AlgoTools.IO;

/**  Verzweigung durch Fallunterscheidung (switch/case-Anweisung) 
 *
 */  

public class Fall {

  public static void main (String [] argv) {

    int zahl  = 42;
    int monat = 11;

   switch (zahl % 10) {// verzweige in Abhaengigkeit der letzten Ziffer von zahl

        case 0:  IO.println("null  "); break;
        case 1:  IO.println("eins  "); break;
        case 2:  IO.println("zwei  "); break;
        case 3:  IO.println("drei  "); break;
        case 4:  IO.println("vier  "); break;
        case 5:  IO.println("fuenf "); break;
        case 6:  IO.println("sechs "); break;
        case 7:  IO.println("sieben"); break;
        case 8:  IO.println("acht  "); break;
        case 9:  IO.println("neun  "); break;
    }


    switch(monat) {                  // verzweige in Abhaengigkeit von monat
        
        case  3: case  4: case  5:   IO.println("Fruehling"); break;        
        case  6: case  7: case  8:   IO.println("Sommer   "); break;        
        case  9: case 10: case 11:   IO.println("Herbst   "); break;        
        case 12: case  1: case  2:   IO.println("Winter   "); break;        
        default:                     IO.println("unbekannte Jahreszeit"); 
    }
                     
  }
}
