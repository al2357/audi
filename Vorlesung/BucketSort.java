/***************************  BucketSort.java  ********************************/

import AlgoTools.IO;

/** Sortieren durch Verteilen auf Buckets (Faecher).
 *  Idee: 1.) Zaehlen der Haeufigkeiten b[i] einzelner Schluessel i;
 *        2.) Buckets durchlaufen und i-ten Schluessel b[i]-mal ausgeben.
 */

public class BucketSort {

  private static final int N = 256;             // Alphabetgroesse N

  public static void sort (char[] a) {          // sortiere Character-Array a
    
    int[] b = new int[N];                       // N Buckets
    int i, j, k;                                // Laufvariablen

    for (i=0; i < N; i++) b[i] = 0;             // setze alle Buckets auf 0

    for (i=0; i < a.length; i++)                // fuer jedes Eingabezeichen
        b[a[i]]++;                              // zustaendiges Bucket erhoehen
    
    k = 0;
    for (i=0; i < N; i++)                       // fuer jedes Bucket
        for (j=0; j < b[i]; j++)                // gemaess Zaehlerstand
            a[k++] = (char) i;                  // sein Zeichen uebernehmen 

  }

  public static void main (String [] argv) {
 
    char[] zeile;                                // Zeichenfolgen    
 
    zeile = IO.readChars("Bitte Zeichenkette:       "); // Zeichenkette einlesen
    sort(zeile);                                        // Bucket-Sort aufrufen
    IO.print("sortiert mit Bucket-Sort: ");             // Ergebnis ausgeben
    IO.println(zeile);
  }
}
// Aufwand: O(n) + O(N)   bei n(=a.length) zu sortierenden Zeichen
//                        aus einem N-elementigen Alphabet
