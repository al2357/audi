/***************************  CharKeller.java  ********************************/
/** Interface fuer den ADT CharKeller                                         */

public interface CharKeller extends Keller {
  public void push(char c);     // legt char auf den Keller
  public char ctop();           // liefert oberstes Kellerelement als char
}
