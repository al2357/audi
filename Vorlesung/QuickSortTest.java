/***************************  QuickSortTest.java  *****************************/

import AlgoTools.IO;

/** testet Quicksort
 */

public class QuickSortTest {

  public static void main (String [] argv) {

    int[] a;                                            // Folge a
    
    a = IO.readInts("Bitte eine Zahlenfolge: ");        // Folge einlesen
    QuickSort.sort(a);                                  // QuickSort aufrufen
    IO.print("sortiert mit QuickSort:");
    for (int i=0; i<a.length; i++) IO.print(" "+a[i]);  // Ergebnis ausgeben
    IO.println();
  }
}
