/*****************************  Bedingung.java  *******************************/

import AlgoTools.IO;

/**  Verzweigung durch Bedingung (if-Anweisung, Bedingungsoperator)
 *
 *   Vergleichsoperatoren:   <    kleiner                  
 *                           <=   kleiner gleich      
 *                           ==   gleich             
 *                           >    groesser          
 *                           >=   groesser gleich  
 *                           !=   ungleich        
 *                                              
 *   logische Operatoren:    &&   und           
 *                           ||   oder  
 *                           ^    exklusives oder
 *                           !    nicht       
 */  

public class Bedingung {

  public static void main (String [] argv) {

    int x = -5, y, m;                     // definiere 3 Integer-Variablen
                                          // davon eine initialisiert
 
    if (x < 0) x = -x;                    // setze x auf Absolutbetrag
 
    if (x < 0) y = -x; else y = x;        // setze y auf den
                                          // Absolutbetrag von x

    m = IO.readInt("Bitte Monatszahl: ");
    if      ((3 <= m) && (m <=  5)) IO.println("Fruehling");
    else if ((6 <= m) && (m <=  8)) IO.println("Sommer");
    else if ((9 <= m) && (m <= 11)) IO.println("Herbst");
    else if (m==12 || m==1 || m==2) IO.println("Winter");
    else                            IO.println("unbekannte Jahreszeit");
    
    x = x % 2 == 0 ? x/2 : 3*x + 1;       // bedingter Ausdruck:
                                          // weise der linken Seite
                                          // den Ausdruck vor ':' zu,
                                          // falls Bedingung vor '?' wahr;
  }                                       // sonst Ausdruck hinter ':'
}
