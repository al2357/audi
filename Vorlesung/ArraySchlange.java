/*************************** ArraySchlange.java *******************************/

/** Implementation des Interface Schlange mit Hilfe eines Arrays              
 */

public class ArraySchlange implements Schlange {
    
  private Object[] inhalt;                   // Array fuer Schlangenelemente 
  private int head;                          // Index fuer Schlangenanfang
  private int count;                         // Anzahl Schlangenelemente
  
  public ArraySchlange(int N) {              // Konstruktor fuer leere Schlange
    inhalt = new Object[N];                  // besorge Platz fuer N Objekte
    head   = 0;                              // initialisiere Index fuer Anfang
    count  = 0;                              // initialisiere Anzahl
  }

  private boolean full() {                   // Testet, ob Schlange voll ist
    return count == inhalt.length;           // Anzahl gleich Arraylaenge?
  }

  public boolean empty() {                   // Testet, ob Schlange leer ist
    return count == 0;                       // Anzahl gleich 0?
  }

  public void enq( Object x ) {              // Fuegt x hinten ein
    if (full()) throw new RuntimeException(" Schlange ist voll ");
    inhalt[(head+count)%inhalt.length] = x;  // Element einfuegen
    count++;                                 // Anzahl inkrementieren
  }

  public void deq() {                        // Entfernt vorderstes Element
    if (empty()) throw new RuntimeException(" Schlange ist leer ");
    inhalt[head] = null;                     // Verweis auf null setzen
    head = (head + 1) % inhalt.length;       // Anfang-Index weiterruecken
    count--;                                 // Anzahl dekrementieren
  }

  public Object front() {                    // Liefert Element,
    if (empty()) throw new RuntimeException(" Schlange ist leer ");
    return inhalt[head];                     // welches am Anfang-Index steht
  }
}
