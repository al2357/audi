/**************************  Umwandlung.java  *********************************/

import AlgoTools.IO;

/**  implizite und explizite Typumwandlungen zwischen einfachen Datentypen
 */

 
public class Umwandlung {

  public static void main (String [] argv) {

    char   c = '?';                  // das Fragezeichen 
    byte   b = 100;                  // ca.  3 Stellen 
    short  s = 10000;                // ca.  5 Stellen
    int    i = 1000000000;           // ca.  9 Stellen
    long   l = 100000000000000000L;  // ca. 18 Stellen 
    float  f = 3.14159f;             // ca.  6 Stellen Genauigkeit
    double d = 3.141592653589793;    // ca. 15 Stellen Genauigkeit

    i = s ;                          // implizite Typumwandlung ohne Verlust
    i = c ;                          // implizite Typumwandlung ohne Verlust
    s = (short) c;                   // explizite Typumwandlung ohne Verlust 
    s = (short) i;                   // explizite Typumwandlung  mit Verlust
    d = i;                           // implizite Typumwandlung ohne Verlust
    i = (int) d;                     // explizite Typumwandlung  mit Verlust
    
    d = f;                           // implizite Typumwandlung ohne Verlust
    f = (float) d;                   // explizite Typumwandlung  mit Verlust

    d = 1/2;                         // ergibt 0 wegen ganzzahliger Division
    IO.println(d);
    d = 1/2.0;                       // ergibt 0.5 wegen Gleitkommadivision
    IO.println(d);

    IO.println(2 * b + c );          // Ausdruck ist vom Typ int, Wert = 263
    IO.println(i + 1.5);             // Ausdruck ist vom Typ double

  }
}
