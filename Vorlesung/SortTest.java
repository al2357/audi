/***************************  SortTest.java  **********************************/

import AlgoTools.IO;

/** testet Sortierverfahren
 */

public class SortTest {

  public static void main (String [] argv) {

    int[] a, b, c;                                      // Folgen a, b und c
    
    a = IO.readInts("Bitte eine Zahlenfolge:      ");   // Folge einlesen
    SelectionSort.sort(a);                              // SelectionSort aufr.
    IO.print("sortiert mit SelectionSort: ");
    for (int i=0; i<a.length; i++) IO.print(" "+a[i]);  // Ergebnis ausgeben
    IO.println();
    IO.println();

    b = IO.readInts("Bitte eine Zahlenfolge:      ");   // Folge einlesen
    BubbleSort.sort(b);                                 // BubbleSort aufrufen
    IO.print("sortiert mit BubbleSort:    ");
    for (int i=0; i<b.length; i++) IO.print(" "+b[i]);  // Ergebnis ausgeben
    IO.println();
    IO.println();

    c = Merge.merge(a,b);                               // mische beide Folgen
    IO.print("sortierte Folgen gemischt:  ");
    for (int i=0; i<c.length; i++) IO.print(" "+c[i]);  // Ergebnis ausgeben
    IO.println();
  }
}
