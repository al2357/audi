/**************************  Konstanten.java  *********************************/

import AlgoTools.IO;

/**  Einsatz von Konstanten fuer ganze Zahlen, Gleitkomma und Character 
 */

 
public class Konstanten {

  public static void main (String [] argv) {

    final int    MAX_GRAD = 360;               // Integer-Konstante
    final double PI       = 3.141592653589793; // Double Konstante 
    final char   PIEP     = (char) 7;          // Character-Konstante 

    int grad;
    double r;
    
    IO.print(PIEP);                            // erzeuge Piep

    for (grad=0; grad <= MAX_GRAD; grad++) {   // fuer jeden ganzzahligen Winkel
        r = Math.sin (grad/360.0*2*PI);        // berechne Sinus vom Bogenmass
        IO.println(grad + " " + r);            // gib Winkel und Sinus aus
    }
  }
}
