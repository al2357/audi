/*****************************  AVLBaumTest.java  *****************************/

import AlgoTools.IO; 

/** Klasse zum Testen des AVLBaums: Einfuegen und Loeschen von Character
 */

public class AVLBaumTest {

  public static void main(String[] argv) {

    AVLBaum b = new AVLBaum();
    
                                         // In den AVLBaum einfuegen
    char k = IO.readChar("Char in AVL-Baum einfuegen (Loeschen: \\n): ");
    
    while (k != '\n') {
      if (b.insert(new Character(k)))
        IO.println(k + " eingefuegt");
      else                            
        IO.println(k + " nicht eingefuegt");
      IO.println("AVL-Baum mit Balancen:");
      AVLBaum.printAVLBaum(b, 0);
      k = IO.readChar("Char in AVL-Baum einfuegen (Loeschen: \\n): ");
    }
    
    IO.println();
    
                                         // Aus dem AVLBaum loeschen
    k = IO.readChar("Char im AVL-Baum loeschen (Abbruch: \\n): ");

    while (k != '\n') {
      if (b.delete(new Character(k)))
        IO.println(k + " geloescht");
      else                            
        IO.println(k + " nicht geloescht");
      IO.println("AVL-Baum mit Balancen:");
      AVLBaum.printAVLBaum(b, 0);
      k = IO.readChar("Char im AVL-Baum loeschen (Abbruch: \\n): ");
    }
  }
}
