/************************* UndiVertex.java ************************************/
import java.util.*;
public class UndiVertex {

  public String         name;       // Name des Knoten               (fix)
  public List<UndiEdge> edges;      // inzidente Kanten              (fix)
  public UndiVertex     chef;       // Repraesentant fuer d. Knoten (errechnet)
  
  public UndiVertex ( String s ) {          // Konstruktor fuer Knoten
    name = s;                               // initialisiere Name des Knoten 
    edges = new LinkedList<UndiEdge>();     // initialisiere adjazente Kanten 
  }

  public Collection<UndiVertex>neighbors(){ // liefert die Nachbarn des Knotens
    Collection s = new HashSet<UndiVertex>();
    for (UndiEdge e : edges){
      if (e.left  != this) s.add(e.left);
      if (e.right != this) s.add(e.right);
    } return s;
  }

  public boolean hasEdge(UndiVertex w) {    // testet, ob Kante zu w besteht
    for (UndiVertex v : neighbors())        // fuer jede Kante pruefe
      if (v == w)                           // falls n mit w uebereinstimmt
        return true;                        // melde Erfolg
    return false;                           // ansonsten: melde Misserfolg
  }
}
