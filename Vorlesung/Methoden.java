/***************************  Methoden.java  **********************************/

import AlgoTools.IO;

/**  Klassen-Methoden 
 *   mit und ohne formale Parameter
 *   mit und ohne Rueckgabewert 
 */

public class Methoden {

                                             // Methode ohne Rueckgabewert
  public static void bitte() {               // und ohne Parameter         
    IO.println("Bitte Eingabe: "); 
  }

                                             // Methode ohne Rueckgabewert
  public static void sterne(int k) {         // mit einem Integer-Parameter
    int i;                                   // lokale Variable 
    for (i=0; i < k; i++)                    // zeichne k Sterne
        IO.print('*');
    IO.println();
  }

                                             // Methode mit Rueckgabewert
  public static int zweihoch(int n) {        // mit einem Integer-Parametern  
    int i, h = 1;                            // lokale Variablen            
    for (i=0; i < n; i++) h = h * 2;         // berechne 2 hoch n
    return h;                                // liefere Ergebnis ab
  }

                                             // Methode mit Rueckgabewert
  public static int ggt(int a, int b) {      // zwei Integer-Parameter
    while (a != b)                           // solange Zahlen verschieden 
        if (a > b) a=a-b; else b=b-a;        // ziehe kleinere von groesserer ab
    return a;                                // liefere Ergebnis zurueck
  }                                          // aktuelle Parameter unveraendert

                                             // Methode ohne Rueckgabewert
  public static void main (String [] argv) { // oeffentlich aufrufbar   
    bitte();                                 // rufe bitte auf
    sterne(3*5+7);                           // rufe sterne auf
    IO.println("2 hoch 7 ist " + zweihoch(7));// rufe zweihoch auf
    IO.println("ggt(28,12) = " + ggt(28,12));// rufe ggt auf
  }
}
