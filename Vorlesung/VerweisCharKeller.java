/************************  VerweisCharKeller.java  ****************************/
/** Abstrakter Datentyp Character-Keller mit Elementen vom Typ char           */

public class VerweisCharKeller extends VerweisKeller implements CharKeller {

  public void push(char x) {                   // legt char x auf den Keller
    push(new Character(x));
  }

  public char ctop() {                         // liefert oberstes Kellerelement
    return ((Character)top()).charValue();                            
  }
}
