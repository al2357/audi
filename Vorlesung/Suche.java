/***************************  Suche.java  *************************************/
import AlgoTools.IO;

/**  lineare Suche eines Wertes und des Minimums im Array
 *   und binaere Suche im geordneten Array 
 */

public class Suche {

  public static void main (String [] argv) {

    int[] a;                                         // Feld fuer Zahlenfolge
    int i, x, min, index, links, rechts, mitte;

    a = IO.readInts("Bitte Zahlenfolge:      ");     // bestimme das Minimum
    min = a[0]; index = 0;                           // Minimum ist erster Wert
    for (i = 1; i < a.length; i++){                  // durchlaufe gesamte Folge
        if (a[i] < min) {                            // falls neues Minimum 
            index = i;                               // merke Position 
            min   = a[i];}                           // und Wert des Minimums
    } 
    IO.print("Minimum = " + min);                    // gib Ergebnis bekannt
    IO.println(" an Position " + index);

    a = IO.readInts("Bitte Zahlenfolge:      ");     // suche in einer Folge
    x = IO.readInt ("Bitte zu suchende Zahl:      ");// ein bestimmtes Element 
    for (i=0; (i < a.length) && (x != a[i]); i++);   // durchlaufe Folge
    if (i==a.length) IO.println("Nicht gefunden");   // gib Suchergebnis bekannt
    else IO.println("Gefunden an Position " + i);
 
    a = IO.readInts("Bitte sortierte Zahlenfolge: ");// suche in sortierter Folge
    x = IO.readInt ("Bitte zu suchende Zahl:      ");// ein bestimmtes Element 
    links  = 0;                                      // initialisiere links
    rechts = a.length-1;                             // initialisiere rechts
    mitte  = (links + rechts)/2;                     // initialisiere mitte
    while (links <= rechts && a[mitte] != x) {       // solange noch Hoffnung 
        if (a[mitte] < x)  links  = mitte+1;         // zu kurz gesprungen
        else               rechts = mitte-1;         // zu weit gesprungen
        mitte = (rechts+links)/2;                    // neue Mitte
    }    
    if (links > rechts) IO.println("Nicht gefunden");// gib Ergebnis bekannt
    else IO.println("Gefunden an Position " + mitte);
  }
}
