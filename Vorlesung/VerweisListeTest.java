/************************ VerweisListeTest.java  ******************************/

import AlgoTools.IO;

/** Testet die Implementation der VerweisListe            
 */

public class VerweisListeTest {

  public static void main (String [] argv) {

    Liste l = new VerweisListe();                          // kreiere Liste
    Student s;                                             // deklariere Student

    s = new Student("Willi","Wacker",22,8,1972,"BWL",1995);// kreiere Student  
    l.insert(s);                                           // fuege in Liste ein
    l.advance();                                           // eins weiter in l

    s = new Student("Erika","Muster",28,2,1970,"VWL",1994);// kreiere Student  
    l.insert(s);                                           // fuege in Liste ein
    l.advance();                                           // eins weiter in l

    s = new Student("Hein","Bloed",18,5,1973,"CLK",1996);  // kreiere Student  
    l.insert(s);                                           // fuege in Liste ein
    l.advance();                                           // eins weiter in l

    s = new Student("Susi","Sorglos",10,7,1973,"JUR",1996);// kreiere Student  
    l.insert(s);                                           // fuege in Liste ein

    l.reset();                                             // an den Anfang

    while (!l.endpos()) {                                  // 1.,3.,5.. loeschen
      l.delete(); 
      if (!l.endpos()) 
        l.advance();
    }

    l.reset();                                             // an den Anfang
    while (!l.endpos()) {                                  // Liste abarbeiten 
      IO.println(l.elem());                                // Student drucken
      l.advance();                                         // einmal vorruecken 
    }

    l.reset();                                             // an den Anfang
    while (!l.empty()) l.delete();                         // Liste loeschen
  }
}
