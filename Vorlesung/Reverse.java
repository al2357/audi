/*****************************  Reverse.java  *********************************/

import AlgoTools.IO;

/** Liest eine Folge von ganzen Zahlen ein 
 *  und gibt sie in umgekehrter Reihenfolge wieder aus. 
 *  Verwendet wird die Wrapper-Klasse Integer, 
 *  welche Objekte vom einfachen Typ int enthaelt. 
 *  Vor dem Einfuegen in den Keller werden mit new Integer diese Objekte
 *  erzeugt, nach dem Auslesen aus dem Keller werden sie nach int gecastet.
 */

public class Reverse {

  public static void main (String [] argv) {

    Keller k = new VerweisKeller();                 // lege leeren Keller an

    int[] a = IO.readInts("Bitte Zahlenfolge:   "); // lies Integer-Folge ein
    
    for (int i=0; i<a.length; i++)                  // pushe jede Zahl als 
        k.push(new Integer(a[i]));                  // Integer-Objekt 

    IO.print("Umgekehrte Reihenfolge:");
    while (!k.empty()) {                            // solange Keller nicht leer
        IO.print(" "+((Integer)k.top()).intValue());// gib Top-Element aus
        k.pop();                                    // entferne Top-Element
    }
    IO.println();
  }
}
