/**************************  Gleitkomma.java  *********************************/

import AlgoTools.IO;

/**  Gleitkommaoperationen 
 */
 
public class Gleitkomma {

  public static void main (String [] argv) {

    double summe, summand, pi, nenner, x;  // fuenf 64-Bit-Gleitkommazahlen

    IO.println("1/2 + 1/4 + 1/8 + ...");
    summe = 0.0; summand = 1.0;            // Initialisierungen
    while (summand > 0.0000000000001) {    // solange Summand gross genug
        summand = summand / 2.0;           // teile Summand
        summe   = summe + summand;         // erhoehe Summe
        IO.println(summe, 22, 16);         // drucke Summe auf insgesamt
    }                                      // 22 Stellen mit 16 Nachkommastellen

                                           // Pi/4 = 1 -1/3 +1/5 -1/7 ...
                                           // Pi   = 1 -4/3 +4/5 -4/7 ...
    summe  = 4.0;                          // setze Naeherung auf 4
    nenner = 1.0;                          // setze nenner auf 1
    IO.println("Naeherung von Pi :");      // gib Naeherungswerte aus
    for (int i=0; i <= 10000; i++) {       // tue 10000 mal
        nenner += 2.0;                     // erhoehe nenner um 2
        summand = 4.0 / nenner;            // bestimme naechsten summand
        if (i%2 == 0) summand = -summand;  // abwechselnd positiv/negativ
        summe = summe + summand;           // addiere summand auf Naeherung 
        IO.println(summe, 22, 16);         // drucke Naeherung auf insgesamt 
                                           // 22 Stellen, 16 Nachkommastellen 
    }                                       

    IO.println("Ueberlaufdemo :");
    x = 2.0;                               // setze x auf 1
    while (x < Double.MAX_VALUE) {         // solange kleiner als Maximum
        x = x * x;                         // quadriere x 
       IO.println(x,30);                      // drucke in wiss. Notation 
    }                                      // druckt als letzten Wert Infinity
 
    IO.println("Teilung durch 0:");
    IO.println( -1.0 / 0.0 );              // ergibt -Infinity
    IO.println(  0.0 / 0.0 );              // ergibt NaN (not a number)

  }
}
