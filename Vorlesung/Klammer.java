/*****************************  Klammer.java  *********************************/

import AlgoTools.IO;

/** Ueberprueft Klammerung mit Hilfe eines Kellers 
 *  und markiert die erste fehlerhafte Position
 */

public class Klammer {

  public static void main(String[] argv) {

    char[] zeile;                         // Eingabezeichenkette
    int i = 0;                            // Laufindex in char[] c
    boolean fehler = false;               // Abbruchkriterium
    Keller k = new VerweisKeller();       // Keller fuer Zeichen

    zeile = IO.readChars("Bitte Klammerausdruck eingeben: ");
    IO.print("                               ");

    for (i=0; i < zeile.length && !fehler; i++){

      switch (zeile[i]) {

        case '(':                                        // oeffnende Klammer 
        case '[': k.push(new Character(zeile[i])); break;// auf den Keller

        case ')': if (!k.empty() &&                      // runde Klammer zu
                  ((Character)k.top()).charValue()=='(') // ueberpruefen
                       k.pop();                          // und vom Keller 
                  else fehler = true; break;             // sonst Fehler
        
        case ']': if (!k.empty() &&                      // eckige Klammer zu
                  ((Character)k.top()).charValue()=='[') // ueberpruefen 
                       k.pop();                          // und vom Keller 
                  else fehler = true; break;             // sonst Fehler

        default:                                         // beliebiges Zeichen
      }
      IO.print(" ");                                     // weiterruecken in 
    }                                                    // der Ausgabe

    if (!fehler && k.empty()) 
         IO.println("korrekt geklammert");
    else IO.println("^ nicht korrekt geklammert");
  }
}
