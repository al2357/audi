public class SelectionSort {                   // Klasse SelectionSort
  
  public static void sort(int[] a) {           // statische Methode sort

    int i, j, pos, min;                        // 2 Indizes, Position, Minimum
   
    for (i=0; i<a.length-1; i++) {             // durchlaufe Array
        pos = i;                               // Index des bisher kleinsten
        min = a[i];                            // Wert des bisher kleinsten
        for (j=i+1; j<a.length; j++)           // durchlaufe Rest des Array
            if (a[j] < min) {                  // falls kleineres gefunden,
                pos = j;                       // merke Position des kleinsten 
                min = a[j];                    // merke Wert des kleinsten 
            }
        a[pos] = a[i];                         // speichere bisher kleinstes um
        a[i]   = min;                          // neues kleinstes nach vorne
    }
  }
}
