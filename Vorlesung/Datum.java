/***************************  Datum.java  *************************************/

/** Klasse Datum 
 *  bestehend aus drei Integers (Tag, Monat, Jahr) 
 *  und zwei Konstruktoren zum Anlegen eines Datums
 *  und einer Methode zur Umwandlung eines Datums in einen String
 */

public class Datum {

  int tag;                                     // Datenfeld tag 
  int monat;                                   // Datenfeld monat 
  int jahr;                                    // Datenfeld jahr

  public Datum (int t, int m, int j){          // Konstruktor mit 3 Parametern
    tag   = t;                                 // initialisiere Tag 
    monat = m;                                 // initialisiere Monat
    jahr  = j;                                 // initialisiere Jahr
  }

  public Datum (int jahr){                     // Konstruktor mit 1 Parameter
    this(1, 1, jahr);                          // initialisiere 1.1. Jahr
  }

  public String toString(){                    // Methode ohne Parameter
    return tag + "." + monat + "." + jahr;     // liefert Datum als String
  }

}
