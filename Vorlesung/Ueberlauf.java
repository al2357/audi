/**************************  Ueberlauf.java ***********************************/
import AlgoTools.IO;

/**  Integer-Ueberlauf */
 
public class Ueberlauf {

  public static void main (String [] argv) {

    int n = 1;                            // initialisiere n
    while (n > 0) {                       // solange n positiv ist
        n = n * 10;                       // verzehnfache n
        IO.println(n, 20);                // drucke n auf 20 Stellen
    }                                     // letzter Wert ist negativ ! 

  } 
}
