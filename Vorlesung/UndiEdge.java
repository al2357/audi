/*********************** UndiEdge.java ****************************************/
import java.util.*;
public class UndiEdge implements Comparable<UndiEdge>{

  public UndiVertex left;             // der eine benachbarte Knoten 
  public UndiVertex right ;           // der andere benachbarte Knoten 
  public double cost;                 // Kosten dieser Kante
  public boolean status;              // Zustand der Kante  (errechnet)
  
  public UndiEdge (UndiVertex l, UndiVertex r, double c) { // Konstruktor 
    left  = l;                        // initialisiere den einen Nachbarn
    right = r;                        // initialisiere den anderen Nachbarn
    cost  = c;                        // initialisiere Kantenkosten
    status = false;                   // Kante ist noch unbearbeitet
  }

  public int compareTo (UndiEdge other) { // vergleiche mit anderer Kante
    return (int)(cost - other.cost);  // liefere Ergebnis des Vergleichs
  }
}
