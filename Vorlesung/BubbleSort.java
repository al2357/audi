public class BubbleSort {                      // Klasse BubbleSort

  public static void sort(int[] a) {           // statische Methode sort
    int tmp;                                   // Hilfsvariable zum Tauschen
    boolean getauscht;                         // merkt sich, ob getauscht
    do {
        getauscht = false;                     // nimm an, dass nicht getauscht
        for (int i=0; i<a.length-1; i++){      // durchlaufe Array
            if (a[i] > a[i+1]) {               // falls Nachbarn falsch herum
                tmp = a[i];                    // bringe 
                a[i] = a[i+1];                 // beide Elemente
                a[i+1] = tmp;                  // in die richtige Ordnung
                getauscht = true;              // vermerke, dass getauscht
            }
        }
    } while (getauscht);                       // solange getauscht wurde
  }
} 
