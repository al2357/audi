/***************************  PersonStudentTest.java **************************/

import AlgoTools.IO;

/** Klasse PersonStudentTest
 *  verwendet Instanzen der Klasse Person und der Klasse Student
 */

public class PersonStudentTest {

  public static void main (String [] argv) {                  

    Student s;                                           // Student
    Person p;                                            // Person

    p = new Person("Uwe","Meier",24,12,1971);            // kreiere Person
    s = new Student("Eva","Kahn",15,9,1972,"BWL",1998);  // kreiere Student  

    IO.println(p + "'s Jahrgang: " + p.jahrgang());      // gib Jahrgang aus
                                                         // da p eine Person ist
                                                         // wird Geburtsjahrgang
                                                         // ausgegeben

    IO.println(s + "'s Jahrgang: " + s.jahrgang());      // gib Jahrgang aus
                                                         // da s ein Student ist
                                                         // wird
                                                         // Immatrikulationsjahr
                                                         // ausgegebn

    p = s;                                               // p zeigt auf s

    IO.println(p + "'s Jahrgang: " + p.jahrgang());      // gib Jahrgang aus
                                                         // da p auf Student 
                                                         // zeigt, wird
                                                         // Immatrikulationsjahr
                                                         // ausgegeben
                                                     

    
    if (p instanceof Student)                            // falls p Student ist
       IO.println(((Student)p).fach);                    // gib p's Fach aus 

    s = (Student) p;                                     // s zeigt auf p 
    IO.println(s.fach);                                  // gib s's Fach aus
  }
}
