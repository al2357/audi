/***************************  Feld.java  **************************************/

import AlgoTools.IO;

/**  Zusammenfassung mehrerer Daten desselben Typs zu einem Feld 
 */

public class Feld {

  public static void main (String [] argv) {

    double[] a;                                  // eindimensionales double-Feld
    int i, j;                                    // Laufvariablen

    a = new double[5];                           // besorge Platz fuer 5 Double 

    for (i=0; i < a.length; i++)                 // durchlaufe das Feld
        IO.println(a[i]);                        // drucke jedes Feldelement

    double[][] m = new double[4][3];             // 4x3 Matrix vom Typ double

    for (i=0; i<m.length; i++) {                 // durchlaufe jede Zeile
        for (j=0; j < m[i].length; j++)          // durchlaufe die Spalten
            IO.print(m[i][j], 8, 2);             // drucke Matrixelement
        IO.println();                            // gehe auf neue Zeile
    }

    int[][] dreieck = {{1}, {2,3}, {4,5,6}};     // untere linke Dreiecksmatrix 
  }
}
