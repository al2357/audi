/*****************************  TraverseTest.java  ****************************/
                                                         //         '/'
import AlgoTools.IO;                                     //      /       \
                                                         //      +       -
/** Traversierungen des binaeren Baums mit Operanden in  //    /   \   /   \
 *  den Blaettern und Operatoren in den inneren Knoten:  //    F   *   X   Y
 */                                                      //       / \
                                                         //       A B
public class TraverseTest {

  public static void main(String[] argv) {

    VerweisBaum a = new VerweisBaum(new Character('A'));
    VerweisBaum b = new VerweisBaum(new Character('B'));
    VerweisBaum m = new VerweisBaum(a, new Character('*'), b);
    VerweisBaum f = new VerweisBaum(new Character('F'));
    VerweisBaum p = new VerweisBaum(f, new Character('+'), m);
    VerweisBaum x = new VerweisBaum(new Character('X'));
    VerweisBaum y = new VerweisBaum(new Character('Y'));
    VerweisBaum n = new VerweisBaum(x, new Character('-'), y);
    VerweisBaum d = new VerweisBaum(p, new Character('/'), n);

    IO.print("Preorder:        ");
    Traverse.preorder(d);          IO.println(); // Ausgabe: /+F*AB-XY

    IO.print("Inorder:         ");
    Traverse.inorder(d);           IO.println(); // Ausgabe: F+A*B/X-Y

    IO.print("Postorder:       ");
    Traverse.postorder(d);         IO.println(); // Ausgabe: FAB*+XY-/

    IO.print("Klammer-Inorder: ");
    Traverse.klammerinorder(d);    IO.println(); // Ausgabe: ((F+(A*B))/(X-Y))

    IO.print("Tiefensuche:     ");
    TiefenSuche.tiefenSuche(d);    IO.println(); // Ausgabe: /+F*AB-XY

    IO.print("Breitensuche:    ");
    BreitenSuche.breitenSuche(d);  IO.println(); // Ausgabe: /+-F*XYAB

  }
}
