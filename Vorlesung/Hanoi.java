/***************************  Hanoi.java  *************************************/

import AlgoTools.IO;

/**  Tuerme von Hanoi:
 *   n Scheiben mit abnehmender Groesse liegen auf dem Startort A. 
 *   Sie sollen in derselben Reihenfolge auf Zielort C zu liegen kommen.
 *   Die Regeln fuer den Transport lauten:
 *   1.) Jede Scheibe muss einzeln transportiert werden.
 *   2.) Es darf nie eine groessere Scheibe auf einer kleineren liegen.  
 *   3.) Es darf ein Hilfsort B zum Zwischenlagern verwendet werden.
 */

//            |                       |                        |
//           x|x                      |                        |
//          xx|xx                     |                        |
//         xxx|xxx                    |                        |
//        xxxx|xxxx                   |                        |
//     ---------------         ---------------          ---------------
//         Start (A)              Zwischen (B)                Ziel (C)

public class Hanoi {

  static void verlege (           // drucke die Verlegeoperationen, um
    int n,                        // n Scheiben
    char start,                   // vom Startort
    char zwischen,                // unter Zuhilfenahme eines Zwischenortes
    char ziel) {                  // zum Ziel zu bringen

    if (n>0) {
        verlege(n-1,start, ziel, zwischen);
        IO.println("Scheibe " + n +" von " + start + " nach " + ziel);
        verlege(n-1,zwischen, start, ziel);
    }
  }

  public static void main (String [] argv) {
    int n;
    do{ n = IO.readInt("Bitte Zahl der Scheiben (n>0): "); } while (n <= 0);
    verlege(n,'A','B','C');
  }
}
