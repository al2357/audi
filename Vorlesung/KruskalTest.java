/************************* KruskalTest.java************************************/

import java.util.*;
import AlgoTools.IO;

/** testet den Kruskalgorithmus auf einem ungerichteten Graphen               */

public class KruskalTest {

  public static void main(String [] argv) {
    
    UndiGraph g = UndiGraphIO.readUndiGraph();      // Graph einlesen
  
    UndiGraphIO.printUndiGraph(g);                  // Graph ausgeben

    Kruskal.kruskal(g);                             // berechne Spannbaum
 
    IO.print("Der minimale Spannbaum ");            // gib den errechneten 
    IO.println("besteht aus folgenden Kanten:\n "); // Spannbaum aus
    for (UndiEdge e : g.edges()){                   // durchlaufe alle Kanten 
      if (e.status) 
        IO.println("(" + e.left.name + "," + e.right.name + ") " + e.cost);
    } 
  }
}
