/***************************  Ziffern.java  ***********************************/

import AlgoTools.IO;

/**  Erwartet eine Folge von Ziffern
 *   und berechnet den zugehoerigen Wert.
 */

public class Ziffern {

  public static void main (String [] argv) {

    char[] zeile;                           // Array fuer Zeile
    char c;                                 // Character-Variable

    int i , wert, ziffernwert;              // Hilfsvariablen 

    zeile = IO.readChars("Bitte Ziffernfolge: "); // Liest so lange Zeichen von
                                            // der Tastatur, bis <Return>
    wert = 0;
    for (i=0; i < zeile.length; i++) {
        c = zeile[i];                       // besorge naechstes Zeichen
        ziffernwert = (int) c - (int) '0';  // konvertiere Ziffer zur Zahl
        wert = 10*wert + ziffernwert;       // verknuepfe mit bisherigem Wert
    }
    IO.println("Der Wert lautet " + wert);  // gib Wert aus
  }
}
