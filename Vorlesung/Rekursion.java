/***************************  Rekursion.java  *********************************/

import AlgoTools.IO;

/** Rekursive Methoden                                                        */

public class Rekursion {

  public static int fakultaet (int n) {  // 
    if (n == 0)                          //                               
        return 1;                        //           1         falls n=0
    else                                 //  n! :=                    
        return n * fakultaet(n-1);       //           n*(n-1)!  sonst
  }

  public static int zweihoch (int n) {   //
    if (n == 0)                          //           1         falls n=0
        return 1;                        //   n   
    else                                 //  2   :=      (n-1)
        return 2*zweihoch(n-1);          //           2*2       sonst 
  }

  public static int fib (int n){         // Jedes Kaninchenpaar bekommt vom
    if (n <=1 )                          // 2. Jahr an ein Paar als Nachwuchs
        return 1;                        //                                     
    else                                 // Jahr     0  1  2  3  4  5  6  7  8
        return fib(n-1) + fib(n-2);      // Paare    1  1  2  3  5  8 13 21 34
  }                                      //
 
  public static int ggt (int x, int y) { //
    if (y == 0)                          //                                  
        return x;                        //             x          falls y=0
    else                                 // ggt(x,y) :=                  
        return ggt(y, x % y);            //             ggt(y,x%y) sonst
  }

  public static void main (String [] argv) {
    int[] a = IO.readInts("Bitte zwei Zahlen: ", 2);
    IO.println(a[0] + " ! =  " + fakultaet(a[0]));
    IO.println("2 hoch " + a[0] + " = " + zweihoch(a[0]));
    IO.println(a[0] + ". Fibonacci-Zahl = " + fib(a[0]));
    IO.println("ggt(" + a[0] + "," + a[1] + ") = " + ggt(a[0],a[1]));
  }
}
