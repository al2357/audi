/***************************  Parameter.java  *********************************/

import AlgoTools.IO;

/**  Uebergabe von Arrays an Methoden
 */

public class Parameter {
                                             // Methode ohne Rueckgabewert
  public static void zeige(int[] a) {        // erhaelt Array als Parameter
    for (int i=0; i < a.length; i++)         // gibt Array aus 
        IO.print(a[i],5);
    IO.println();
  } 
                                             // Methode mit Rueckgabewert
  public static int summe(int[] a) {         // erhaelt Array als Parameter
    int s = 0;                               // initialisiert Summe
    for (int i=0; i< a.length; i++)          // durchlaeuft Array 
        s = s + a[i];                        // berechnet Summe
    return s;                                // und liefert sie zurueck
  }
                                             // Methode mit Rueckgabewert
  public static int[] kopiere(int[] a) {     // erhaelt Array als Parameter
    int[] b = new int[a.length];             // besorgt Platz fuer 2. Array
    for (int i=0; i<a.length; i++)           // durchlaeuft Array a
        b[i] = a[i];                         // kopiert es nach b
    return b;                                // liefert b zurueck
  }
                                             // Methode mit Seiteneffekt
  public static void reset(int[] a) {        // erhaelt Array als Parameter
    for (int i=0; i < a.length; i++)         // durchlaeuft Array
        a[i] = 0;                            // und setzt jede Komponente auf 0
  }

  public static void main(String [] argv) {  // erhaelt Strings als Parameter
    int[] folge = {1,4,9,16,25,36};          // deklariere initialisierte Folge 
    int[] noch_eine_folge;                   // deklariere leere Folge
    zeige(folge);                            // gib folge aus
    IO.println("Summe = " + summe(folge));   // gib die Summe aus
    noch_eine_folge = kopiere(folge);        // schaffe eine Kopie der Folge
    zeige(noch_eine_folge);                  // gib Kopie aus
    reset(folge);                            // setze folge auf 0
    zeige(folge);                            // gib folge aus
  }
}
