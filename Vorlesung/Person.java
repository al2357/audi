/***************************  Person.java  ************************************/

/** Klasse Person
 *  bestehend aus Vorname, Nachname, Geburtsdatum 
 *  mit einem Konstruktor zum Anlegen einer Person
 *  und zwei Methoden zum Ermitteln des Jahrgangs und des Alters 
 */

import java.util.GregorianCalendar;

public class Person {

  String vorname;                                  // Datenfeld Vorname 
  String nachname;                                 // Datenfeld Nachname 
  Datum  geb_datum;                                // Datenfeld Geburtsdatum

  public Person (String vn,                        // Konstruktor mit Vorname
                 String nn,                        // Nachname
                 int t,                            // Geburtstag
                 int m,                            // Geburtsmonat
                 int j)                            // Geburtsjahr
  {              
    vorname   = vn;                               // initialisiere Vorname 
    nachname  = nn;                               // initialisiere Nachname
    geb_datum = new Datum(t,m,j);                 // initialisiere Geburtsdatum
  }
  
  public int jahrgang () {                        // Methode
    return geb_datum.jahr;                        // liefert Geburtsjahrgang
  }

  public int alter(){                             // Methode
    int jetzt = new GregorianCalendar().get(GregorianCalendar.YEAR);  
    return jetzt - geb_datum.jahr;                // liefert das Lebensalter
  }

  public String toString() {                      // Methode, ueberschreibt toString
    return vorname + " " + nachname;              // liefert Vor- und Nachname 
  }

}
