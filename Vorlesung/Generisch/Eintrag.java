/******************************* Eintrag.java  ********************************/

/** Implementation eines Eintrags fuer die VerweisListe und den VerweisKeller
 */

public class Eintrag <T> {
	 
  T          inhalt;               // Inhalt des Eintrags
  Eintrag<T> next;                 // Verweis auf naechsten Eintrag
}
