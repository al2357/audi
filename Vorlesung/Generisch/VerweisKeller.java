/***************************  VerweisKeller.java ******************************/

/** Implementation eines Kellers mithilfe von Verweisen
 */

public class VerweisKeller <T> implements Keller <T> {

  private Eintrag<T> top;                        // verweist auf obersten
                                                 // KellerEintrag

  public VerweisKeller() {                       // legt leeren Keller an    	
    top = null;
  }

  public boolean empty() {                       // liefert true,
    return top == null;                          // falls Keller leer
  }

  public void push(T x) {                        // legt Objekt x    
    Eintrag<T> hilf = new Eintrag<T>();          // auf den Keller
    hilf.inhalt     = (T)x;
    hilf.next       = top;
    top             = hilf;
  }

  public T top() {                                // liefert Top-Element 
    if (empty()) throw new 
      RuntimeException(" Keller ist leer ");
    return top.inhalt;                            
  }

  public void pop() {                            // entfernt Top-Element
    if (empty()) throw new 
      RuntimeException(" Keller ist leer ");
    top = top.next;
  }
}
