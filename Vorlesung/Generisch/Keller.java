/*****************************  Keller.java  **********************************/

/** Interface fuer den ADT Keller                                           
 */

public interface Keller <T>{

    public boolean empty();     // liefert true, falls Keller leer, false sonst

    public void push(T x);      // legt Objekt x auf den Keller    

    public T top();             // liefert oberstes Kellerelement

    public void pop();          // entfernt oberstes Kellerelement
}
