/**************************  ArrayAbzaehlreim.java  ***************************/

import AlgoTools.IO;

/** n Kinder stehen im Kreis, jedes k-te wird abgeschlagen. 
 *  Die Kreisordnung wird organisiert durch Array mit Indizes der Nachfolger. */ 
public class ArrayAbzaehlreim {

  public static void main (String [] argv) {

    int[] next;                            // Array mit Indizes
    int i, index, n, k;                    // Laufvariablen
    
    n = IO.readInt("Wie viele Kinder ? "); // fordere Zahl der Kinder
    k = IO.readInt("Wie viele Silben ? "); // fordere Zahl der Silben an
    next = new int [n];                    // allokiere Platz fuer Index-Array
 
    for (i = 0; i < n; i++)                // initiale Aufstellung  
        next[i] = (i+1) % n;

    index = n-1;                           // index zeigt auf das Kind vor 
                                           // dem ausgezeichneten Kind     

    while (next[index] != index) {         // so lange abschlagen, bis jemand
                                           // sein eigener Nachfolger ist
        for (i=1; i< k; i++)               // gehe k-1 mal 
            index = next[index];           // zum jeweiligen Nachfolger 

        IO.print("Ausgeschieden: ");       // gib den Index des ausgeschiedenen 
        IO.println(next[index],5);         // naechsten Nachfolgers aus 
        next[index] = next[next[index]];   // setze Index auf neuen Nachfolger
    }
    IO.println("Es bleibt uebrig: " + index);
  }
}
