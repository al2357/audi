/****************************  PostfixPreorderTest.java  *********************/

import AlgoTools.IO;
import java.util.Enumeration;

/** Klasse PostfixPreorderTest konstruiert unter Verwendung von PostfixBaumBau
 *  einen Baum aus dem eingelesenen String und gibt ihn in Preorder-Notation 
 *  aus unter Verwendung von PreorderTraverse                    
 */

public class PostfixPreorderTest {

 public static void main (String [] argv) {

    char[]zeile=IO.readChars("Bitte Postfix-Ausdruck:");// lies Postfixausdruck
    Baum wurzel =PostfixBaumBau.postfixBaumBau(zeile);  // konstruiere daraus 
                                                        // einen Baum
    IO.println("Umwandlung in Prefix-Notation:");
    Enumeration e = new PreorderTraverse(wurzel);       // erzeuge Enumeration
    while (e.hasMoreElements()) {                       // solange Elemente da
      Object x = e.nextElement();                       // besorge Element
      IO.println(x);                                    // gib Element aus
    }                                                   // Obacht: nicht alle
    IO.println();                                       // Objekte eignen sich
  }                                                     // zur Darstellung !
}
