/***************************  Student.java  ***********************************/

/** Klasse Student, spezialisiert die Klasse Person 
 *  durch statische Klassenvariable next_mat_nr;
 *  durch weitere Datenfelder mat_nr, fach, jsb
 *  durch eigenen Konstruktor und durch eigene Methode jahrgang 
 *  welche die Methode jahrgang der Klasse Person ueberschreibt
 */

public class Student extends Person {            // Student erbt von Person  

  static int next_mat_nr = 100000;               // statische Klassenvariable
  int mat_nr;                                    // Matrikel-Nummer
  String fach;                                   // Studienfach
  int jsb;                                       // Jahr des Studienbeginns

  public Student                                 // Konstruktor mit
                 (String vn,                     // Vorname
                  String nn,                     // Nachname 
                  int t,                         // Geburtstag
                  int m,                         // Geburtsmonat
                  int j,                         // Geburtsjahr
                  String f,                      // Studienfach 
                  int jsb)                       // Studienbeginn 
  {  
    super(vn, nn, t, m, j);                      // Konstruktor des Vorfahren
    fach        = f;                             // initialisiere Fach
    this.jsb    = jsb;                           // initialisiere Studienbeginn
    mat_nr      = next_mat_nr++;                 // vergib naechste Mat-Nr.
  }

  public int jahrgang() {                        // Methode liefert als Jahrgang
    return jsb;                                  // das Jahr des Studienbeginns
  }

}
