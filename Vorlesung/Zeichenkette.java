/***************************  Zeichenkette.java  ******************************/

import AlgoTools.IO;

/**  Interpretiert zwei eingelesene Zeichenfolgen als Strings
 *   und vergleicht sie.
 */

public class Zeichenkette {

  public static void main (String [] argv) {

    char[] s, t;                          // Felder von Zeichen
    int i;                                // Laufindex

    s = IO.readChars("Bitte einen String: ");
    t = IO.readChars("Bitte einen String: ");

    IO.print(s);

    for (i=0; i<s.length &&         // solange s noch nicht am Ende 
              i<t.length &&         // solange t noch nicht am Ende
              s[i]==t[i];           // solange beide noch gleich
              i++);                 // gehe eine Position weiter

    if (i==s.length && i==t.length) IO.print("="); // gleichzeitig zu Ende
    else if (i==s.length)           IO.print("<"); // s kuerzer als t
    else if (i==t.length)           IO.print(">"); // t kuerzer als s
    else if (s[i]<t[i])             IO.print("<"); // s kleiner als t
    else                            IO.print(">"); // t kleiner als s

    IO.println(t);
  }
}
