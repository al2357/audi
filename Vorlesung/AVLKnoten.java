/******************************  AVLKnoten.java  ******************************/

/** Klasse AVLKnoten mit einem Konstruktor.
 * 
 *  Die Klasse AVLKnoten erweitert die vorhandene Klasse Knoten um das Datenfeld
 *  balance, die im AVLBaum fuer jeden Knoten abgespeichert wird.   
 */

public class AVLKnoten extends Knoten {
    
  public int balance;                      // Balance

  public AVLKnoten(Object x) {             // erzeugt einen Knoten mit Inhalt x
    super(x);                              // Konstruktoraufruf von Knoten
  }
  
  public String toString() {               // fuer Ausgabe: Inhalt(Balance) 	 
    return inhalt + "(" + balance + ")";
  }
}