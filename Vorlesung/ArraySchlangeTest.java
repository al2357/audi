/**************************** ArraySchlangeTest.java **************************/

import AlgoTools.IO;

/** Programm zum Testen der Methoden des ADT Schlange.
 *  Liest Zeichenketten und reiht sie in eine Schlange ein.
 *  Bei Eingabe einer leeren Zeichenkette wird die jeweils vorderste
 *  aus der Schlange ausgegeben und entfernt.
 */

public class ArraySchlangeTest {

  public static void main(String [] argv) {

    Schlange s = new ArraySchlange(100);          // konstruiere Schlange mit 
                                                  // Platz fuer 100 Objekte
    String eingabe;

    IO.println("Bitte Schlange fuellen durch Eingabe eines Wortes.");
    IO.println("Bitte Schlangen-Kopf entfernen durch Eingabe von RETURN."); 
         
    do {
    
      eingabe = IO.readString("Input: ");         // fordere Eingabe an
      
      if ( eingabe.length()>0 ) {                 // falls Eingabe != RETURN 

          s.enq(eingabe);                         // fuege in Schlange ein
          IO.println(" wird eingefuegt.");

      } else {                                    // falls EINGABE == RETURN

        if (!s.empty()) {
          IO.println(s.front()+" wird entfernt"); // gib Frontelement aus 
          s.deq();                                // entferne Frontelement
        }
      }

    } while (!s.empty());                         // solange Schlange nicht leer 
    
    IO.println("Schlange ist jetzt leer.");       
  }
}
