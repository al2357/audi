/***************************  Schleife.java  **********************************/

import AlgoTools.IO;

/**  while-Schleife, do-while-Schleife, break, continue, for-Schleife
 */

public class Schleife {

  public static void main (String [] argv) {

    int i, x=10, y=2, summe;                  // 4 Integer-Variablen
       
    while (x > 0) {                           // solange x groesser als 0 
        x--;                                  // erniedrige x um eins 
        y = y + 2;                            // erhoehe y um zwei
    }

    do {                                                               
        x++;                                  // erhoehe x um eins
        y += 2;                               // erhoehe y um 2
    } while (x < 10);                         // solange x kleiner als 10 
        
    IO.println("Bitte Zahlen eingeben. 0 als Abbruch");
    summe = 0;                                // initialisiere summe
    x = IO.readInt();                         // lies x ein 
    while (x != 0) {                          // solange x ungleich 0 ist
        summe += x;                           // erhoehe summe
        x = IO.readInt();                     // lies x ein
    }
    IO.println("Die Summe lautet " + summe);

    do {
        x=IO.readInt("Bitte 1<= Zahl <=12");  // lies x ein
    } while (( x < 1) || (x > 12));           // solange x unzulaessig 

    for (i=1; i<=10; i++) IO.println(i*i,6);  // drucke 10 Quadratzahlen
  }
}
