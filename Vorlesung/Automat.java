/***************************  Automat.java  ***********************************/

import AlgoTools.IO;

/**  Endlicher Automat mit delta : Zustand x Eingabe -> Zustand. 
 *   Ueberprueft, ob der eingegebene Binaerstring durch 3 teilbar ist.
 *   Syntaktisch korrekte Eingabe wird vorausgesetzt.   
 *   Zeichen '0' und '1' werden umgewandelt in Integer 0 bzw. 1.
 */

public class Automat {

  public static void main (String [] argv) {

    int[][] delta = {{0,1},                            // Ueberfuehrungsfunktion
                     {2,0},
                     {1,2}};
    int s=0;                                           // Nummer des Zustands

    char[] zeile=IO.readChars("Bitte Binaerstring: "); // fordere Eingabe an
    
    for (int i=0; i < zeile.length; i++)               // fuer jedes Zeichen
    
      s = delta[s][zeile[i]-'0'];                      // wende einmal delta an
                                                       // dabei Zeichen
                                                       // in Zahlen umwandeln
   
    if (s==0) IO.println("ist durch 3 teilbar");       // Automat akzeptiert 
         else IO.println("ist nicht durch 3 teilbar"); // Automat lehnt ab
  }
}
