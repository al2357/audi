/***************************  DatumTest.java **********************************/

import AlgoTools.IO;

/** Klasse DatumTest, testet die Klasse Datum                                 */

public class DatumTest {

  public static void main(String[] argv) {
    Datum d;                                   // deklariere ein Datum
    d = new Datum (15,8,1972);                 // kreiere Datum 15.08.1972 
    d = new Datum (1972);                      // kreiere Datum 01.01.1972 
    d.jahr++;                                  // erhoehe Datum um ein Jahr
    IO.println(d.toString());                  // drucke Datum
    IO.println(d);                             // hier: implizites toString()
  }
}
