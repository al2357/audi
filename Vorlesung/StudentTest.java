/***************************  StudentTest.java ********************************/

import AlgoTools.IO;

/** Klasse StudentTest,  testet die Klasse Student                            */

public class StudentTest { 

  public static void main (String [] argv) {
    Student s;                                             // deklariere Student
    s = new Student("Willi","Wacker",22,8,1972,"BWL",1995);// kreiere Student  
    IO.print(s);                                           // gib Name aus und
    IO.println(" hat die Matrikelnummer " + s.mat_nr);     // Matrikelnummer
  }
}
