/***************************  Matrix.java  ************************************/

import AlgoTools.IO;

/** Multiplikation zweier NxN-Matrizen  
 *
 *       c[i][j]  :=  Summe {k=0 bis N-1}  ( a[i][k] * b[k][j] )
 */

public class Matrix {

  public static void main (String [] argv) {

    final int n = 4;

    int[][] a = {{  1,  2,  3,  4},         // initialisiere Matrix A
                 {  5,  6,  7,  8},
                 {  9, 10, 11, 12},
                 { 13, 14, 15, 16}};

    int[][] b = {{ 17, 18, 19, 20},         // initialisiere Matrix B 
                 { 21, 22, 23, 24},
                 { 25, 26, 27, 28},
                 { 29, 30, 31, 32}};
    
    int[][] c = new int[n][n];              // besorge Platz fuer Matrix

    int i, j, k;                            // Laufindizes 
   
    for (i = 0; i < n; i++)                 // Zeilenindex 
      for (j = 0; j < n; j++) {             // Spaltenindex 
        c[i][j]=0;                          // setze Ergebnis auf 0
        for (k = 0; k < n; k++)             // verknuepfe alle Komponenten 
          c[i][j]=c[i][j]+a[i][k]*b[k][j];  // von Zeile i und Spalte j  
      }       
   }
}
