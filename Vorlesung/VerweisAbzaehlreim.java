/************************ VerweisAbzaehlreim.java *****************************/
import AlgoTools.IO;

/** Klasse VerweisAbzaehlreim
 *  implementiert einen Abzaehlreim mit k Silben fuer n Kinder mit Verweisen
 *  verwendet dabei Objekte vom Typ Kind                                      */

public class VerweisAbzaehlreim {

  public static void main (String [] argv) {

    int i;                                    // Laufvariable
    int n=IO.readInt("Wie viele Kinder ? ");  // erfrage Kinderzahl
    int k=IO.readInt("Wie viele Silben ? ");  // erfrage Silbenzahl
    Kind erster, letzter, index;              // deklariere drei Kinder 
    erster = letzter =  new Kind(0);          // kreiere erstes Kind 

    for (i=1; i < n; i++){                    // erzeuge n-1 mal
      index = new Kind(i);                    // ein Kind mit Nummer i
      letzter.next = index;                   // erreichbar vom Vorgaenger
      letzter = index;
    }

    letzter.next = erster;                    // schliesse Kreis
    index = letzter;                          // beginne bei letztem Kind
    while (index.next != index) {             // solange ungleich Nachfolger
      for (i=1; i<k; i++) index=index.next;   // gehe k-1 mal weiter
      IO.print("Ausgeschieden: ");            // Gib die Nummer des Kindes aus,
      IO.println(index.next.nr, 5);           // welches jetzt ausscheidet
      index.next = index.next.next;           // bestimme neuen Nachfolger
    }
    IO.println("Es bleibt uebrig: " + index.nr); 
  }
}
