/************************* GraphTest.java *************************************/

import java.util.*;

/** testet die Graph-Algorithmen                                              */

public class GraphTest {

  public static void main(String [] argv) {
    
    Graph g = GraphIO.readGraph();             // Graph einlesen
    GraphIO.printGraph(g);                     // Graph ausgeben

    GraphTraverse.tiefensuche(g);              // Tiefensuche-Traversierung
    Result.printTraverse(g);                   // Traversierung ausgeben
 
    TopoSort.sortGraph(g);                     // topologisch sortieren
    Result.printTopo(g);                       // Sortierung ausgeben

    Dijkstra.dijkstra(g, g.getVertex("A"));    // kuerzeste Wege von A berechnen
    Result.printPath(g, g.getVertex("E"));     // kuerzesten Weg zu E ausgeben

    Vertex v = Hamilton.hamilton(g,            // Hamilton-Kreis berechnen
                           g.getVertex("A"));  // dabei bei A beginnen
    Result.printHamilton(g,v);                 // Hamilton-Kreis ausgeben
  }
}
