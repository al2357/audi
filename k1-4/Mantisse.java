import AlgoTools.IO;

public class Mantisse {
	public static void main(String[] arg) {
		int i = 0;
		double dec, mantisse;
		do {
			IO.print("");
			mantisse = IO.readDouble("Geben Sie bitte eine Mantisse zwischen 1 und 2 ein: ");
		} while (mantisse <= 1 || mantisse >= 2);
		dec = mantisse - 1; 
		do {
			dec = dec * 2;
			if(dec < 1) {
				IO.print("0");
			} else {
				IO.print("1");
				dec = dec - 1;
			}
			i++;
		} while (i < 23);
		 
	}
}