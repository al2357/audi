import AlgoTools.*;

public class Primzahltest {
	public static void main (String[] args) {
		int zahl = IO.readInt("Eingabe: ");
		String inf = "eine";
		if(zahl > 1) {
			for(int i = 2; i<zahl; i++) {
				if(zahl % i == 0) {
					inf = "keine";
					break;
				}
			}
			IO.println("Ausgabe: "+zahl+" ist "+inf+" Primzahl.");
		}
	}
}