import AlgoTools.IO;

public class Kapitel4 {
	public static void main (String[] args) {
		//AVar();
		
		double d = 7.0/2.0;
		IO.println(d);
	}
	
	private static void GLZ() {
		double d = 2.5;
		
		double summe = 0.0;
		double summand = 1.0;
		
		while (summand > 0.00000000001) {
			summand = summand / 2.0;
			summe = summe + summand;
			IO.println(summe, 22, 16);
		}
	}
	
	private static void Zeichen () {
		char c = IO.readChar("Bitte ein Zeichen eingeben ");
		
		if(('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z')) {
			IO.println("Das Zeichen ist ein Buchstabe");
		} else {
			IO.println("Das Zeichen ist kein Buchstabe");
		}
	}
	
	private static void ASCIITab () {
		for(int i = 0; i<=255; i++) {
			IO.print(i, 7);
			IO.print(" " + (char) i);
			if(i%8 == 0) IO.println();
		}
	}
	
	private static void AVar () {
		char c;
		
		do {
			c = IO.readChar("Bitte ein Zeichen eingeben ");
			IO.print("Der ASCII code lautet: ");
			IO.println((int)c, 3);
		} while (c != '\n');
	}
}