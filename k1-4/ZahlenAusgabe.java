import AlgoTools.*;

public class ZahlenAusgabe {
	public static void main (String[] args) {
		int zahl = IO.readInt("...");
		if(zahl >= 1) {
			for(int i = 1; i <= zahl; i++) {
				IO.println(i);
			}
		}
	}
}