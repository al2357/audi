/****************************  ShapeTest.java  ********************************/
import AlgoTools.IO;

/**
 * Programmbeschreibung: Testet die Klasse Shape
 */

public class ShapeTest {

  public static void main(String[] argv) {

    IO.println("Erzeuge Shape mit Defaultkonstruktor.");
    IO.println();
    Shape shape = new Shape();

    IO.print("Farbe sollte schwarz sein: ");
    if(shape.colour == "schwarz"){
      IO.println("ERFOLG");
    } else {
      IO.println("FEHLER");
    }

    IO.print("Position sollte im Koordinatenursprung sein: ");
    if(shape.x == 0.0 && shape.y == 0.0 && shape.z == 0.0){
      IO.println("ERFOLG");
    } else {
      IO.println("FEHLER");
    }

    IO.print("Verschiebung um (1.0,1.0,1.0) sollte Position entsprechend anpassen: ");
    shape.move(1.0, 1.0, 1.0);
    if(shape.x == 1.0 && shape.y == 1.0 && shape.z == 1.0){
      IO.println("ERFOLG");
    } else {
      IO.println("FEHLER");
    }

    IO.println();
    IO.println();

    IO.println("Erzeuge Shape mit Customkonstruktor.");
    shape = new Shape("blau", 42.0, 23.0, 73.0);

    IO.println();

    IO.print("Farbe sollte blau sein: ");
    if(shape.colour == "blau"){
      IO.println("ERFOLG");
    } else {
      IO.println("FEHLER");
    }

    IO.print("Position sollte (42.0,23.0,73.0) sein: ");
    if(shape.x == 42.0 && shape.y == 23.0 && shape.z == 73.0){
      IO.println("ERFOLG");
    } else {
      IO.println("FEHLER");
    }
  }
}
