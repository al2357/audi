import AlgoTools.IO;
import java.util.GregorianCalendar;

public class Binding {

	public static void main(String[] args) {
		int i = 1;
		float f = 5.23434f;
		f = i;
		
		Person s = new Student("Alan", "Sk", 10, 01, 1985, "Informatik", 23);
		Person p = new Person("Michael", "Schmidt", 04, 12, 1988);
		IO.println(p.jahrgang());
		p = s;
		IO.println(((Student)p).fach);
	}

}

class Student extends Person {
	static int next_mat_nr = 10000;
	int mat_nr;
	String fach;
	int jsb;
	
	public Student (String vn, String nn, int t, int m, int j, String f, int jsb) {
		super(vn, nn, t, j, m);
		fach = f;
		this.jsb = jsb;
		mat_nr = next_mat_nr++;
	}
	
	public int jahrgang() {
		return jsb;
	}
}

class Person {
	String vorname;
	String nachname;
	Datum geb_datum;
	
	public Person (String vn, String nn, int t, int m, int j) {
		vorname = vn;
		nachname = nn;
		geb_datum = new Datum(t, m, j);
	}
	
	public int jahrgang() {
		return geb_datum.jahr;
	}
	
	public int alter() {
		int jetzt = new GregorianCalendar().get(GregorianCalendar.YEAR);
		return jetzt = geb_datum.jahr;
	}
	
	public String toString() {
		return vorname+" "+nachname;
	}
}

class Datum {
	int tag;
	int monat;
	int jahr;
	
	public Datum(int t, int m, int j) {
		tag = t;
		monat = m; 
		jahr = j;
	}
	
	public Datum(int j) {
		this(1, 1, j);
	}
	
	public String toString() {
		return tag + "." + monat + "." + jahr;
	}
}
