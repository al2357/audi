/****************************  SphereTest.java  ********************************/
import AlgoTools.IO;

/**
 * Programmbeschreibung: Testet die Klasse Sphere
 */

public class SphereTest {
  public static void main(String[] args){

    IO.println("Erstelle schwarze Einheitskugel (r=1, (0.0,0.0,0.0))");
    Sphere sphere = new Sphere("schwarz", 0.0, 0.0, 0.0, 1.0);
    IO.println();

    // colour und move werden hier nicht getestet, da von Shape geerbt.

    IO.print("Teste getVolume(): ");
    if(sphere.getVolume() == (4.0 / 3.0) * Math.PI){
      IO.println("ERFOLGREICH");
    } else {
      IO.println("FEHLGESCHLAGEN");
    }

    IO.print("Teste getArea(): ");
    if(sphere.getArea() ==  4 * Math.PI){
      IO.println("ERFOLGREICH");
    } else {
      IO.println("FEHLGESCHLAGEN");
    }

    IO.print("Setze positiven radius: ");
    sphere.setRadius(10.0);

    if(sphere.getRadius() == 10.0){
      IO.println("ERFOLGREICH");
    } else {
      IO.println("FEHLGESCHLAGEN");
    }

    IO.print("Versuche negativen Radius zu setzen, erwarte RuntimeException: ");
    boolean exceptionThrown = false;

    try {
      sphere.setRadius(-1);
    } catch(RuntimeException e){
      exceptionThrown = true;
    }

    if(exceptionThrown){
      IO.println("ERFOLGREICH");
    } else {
      IO.println("FEHLGESCHLAGEN");
    }

    IO.println();
    IO.println();

    IO.println("Erstelle neue Sphere bei (0.0, 10.0, 0.0) mit Radius 1.0.");
    Sphere sphere2 = new Sphere("schwarz", 0.0, 10.0, 0.0, 1.0);

    IO.println("Setze Radius der ersten Sphere wieder auf 1.0.");
    sphere.setRadius(1.0);

    IO.println();
    IO.print("Der Abstand der Spheren sollte korrekt berechnet werden: ");
    if(sphere.getDistanceTo(sphere2) == 8.0){
      IO.println("ERFOLGREICH");
    } else {
      IO.println("FEHLGESCHLAGEN");
    }

    IO.println();
    IO.print("Erzeuge Shape bei (0.0, 10.0, 0.0).");
    Shape shape = new Shape("schwarz", 0.0, 10.0, 0.0);
    IO.println();

    IO.print("Abstand von der ersten Sphere zum Shape sollte korrekt berechnet werden: ");
    if(sphere.getDistanceTo(shape) == 10.0){
      IO.println("ERFOLGREICH");
    } else {
      IO.println("FEHLGESCHLAGEN");
    }

    IO.println();
    IO.println("Erzeuge Referenz vom Typ Shape mit Sphere 2 als Inhalt.");
    Shape fakeShape = sphere2;
    IO.println();
    IO.print("Abstand von Sphere 1 zu Sphere 2 in der Shape-Referenz sollte korrekt berechnet werden: ");

    if(sphere.getDistanceTo(fakeShape) == 8.0){
      IO.println("ERFOLGREICH");
    } else {
      IO.println("FEHLGESCHLAGEN");
    }



  }
}
