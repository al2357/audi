import AlgoTools.IO;

public class HausAufgabe {
	public static void main(String[] args) {
		Fraglich c = new Fraglich("Wieso jetzt?");
		IO.println(c.var2);
		//System.out.println(b);
	}
}

class Unklar { 

  private String var1;

  public Unklar(String var1){
    this.var1 = var1; // Stelle 1
  }

  public String toString(){
    return "Unklar: " + var1;
  }
}

class Fraglich extends Unklar {
  private int var2;

  public Fraglich(String var1){
    super(var1); // Stelle 2
    var2 = 23;
  }

  public String toString(){
    return "Fraglich: " + var2;
  }
}
