/******************************  Shape.java  **********************************/

/**
 * Diese Klasse repraesentiert eine allgemeine Figur im dreidimensionalen Raum.
 * Jedes Objekt besitzt eine Farbe und eine Position im Raum.
 */
 
public class Shape {

  /**
   * Farbe dieses Shapes
   */
  public String colour;

  /**
   * x-Koordinate dieses Shapes
   */
  public double x;

  /**
   * y-Koordinate dieses Shapes
   */
  public double y;

  /**
   * z-Koordinate dieses Shapes
   */
  public double z;

  /**
   * Instanziiert ein neues Shape mit der Farbe "schwarz"
   * und dem Koordinatenursprung als Position.
   */
  public Shape() {
  // rufen den anderen Konstruktor auf
     this("schwarz", 0.0, 0.0, 0.0);
  }

  /**
   * Instanziiert ein neues Shape mit der uebergebenen Farbe
   * colour und der Position (x, y, z).
   *
   * @param colour
   *          die Farbe des neuen Shape
   * @param x
   *          die x-Koordinate des neuen Shape
   * @param y
   *          die y-Koordinate des neuen Shape
   * @param z
   *          die z-Koordinate des neuen Shape
   */
  public Shape(String colour, double x, double y, double z) {
  // übertragen die Parameter der Methode nach die Attribute der Klasse
    this.colour = colour;
    this.x = x;
    this.y = y;
    this.z = z;
  }

  /**
   * Verschiebt dieses Shape im Raum um dX in x-Richtung, um
   * dY in y-Richtung und um dZ in z-Richtung.
   *
   * @param dX
   *          der Wert, um den dieses Shape in x-Richtung verschoben
   *          wird.
   * @param dY
   *          der Wert, um den dieses Shape in y-Richtung verschoben
   *          wird.
   * @param dZ
   *          der Wert, um den dieses Shape in z-Richtung verschoben
   *          Wird.
   */
  public void move(double dX, double dY, double dZ) {
  // Shape wird in jede Rechunug verschiebt.
    this.x = this.x + dX;
    this.y = this.y + dY;
    this.z = this.z + dZ;
  }

  /**
   * Liefert den Abstand dieses Shape zum uebergebenen Shape
   * other. In dieser Version der Methode wird der Abstand zwischen der
   * durch (x,y,z) gegebenen Position dieses
   * Shape im Raum zu der durch (other.x, other.y,
   * other.z) gegebenen Position des Shape other in
   * euklidischer Metrik berechnet.
   *
   * @param other
   *          das Shape, zu dem der Abstand dieses Shape
   *          berechnet werden soll.
   *
   * @return den Abstand zwischen diesem und dem gegebenen Shape
   */
  public double getDistanceTo(Shape other) {
  // Der Abstand des Shape wird gerechnet.
    double dx = other.x - this.x;
    double dy = other.y - this.y;
    double dz = other.z - this.z;
	// Das Ergebnis wird zurückgegeben.
    return Math.sqrt(dx*dx + dy*dy + dz*dz);
  }

  /**
   * Liefert das Volumen dieses Shape.
   *
   * @return das Volumenen dieses Shape
   */
  public double getVolume() {
    // "Ein allgemeins Objekt im 3D-Raum kann man als Punkt interpretieren. Dieser hat weder Volumen, noch Fläche."
    return 0.0;
  }

  /**
   * Liefert den Flaecheninhalt dieses Shape.
   *
   * @return der Flaecheninhalt dieses Shape
   */
  public double getArea() {
    // "Ein allgemeins Objekt im 3D-Raum kann man als Punkt interpretieren. Dieser hat weder Volumen, noch Fläche." aber... ich möchte das machen.
    double a = Math.sqrt(x*x + z*z);
    double b = Math.sqrt(x*x + y*y);
    double c = Math.sqrt(y*y + z*z);
    double p = (a + b + c)/2;
    
    return Math.sqrt(p * (p-a) * (p-b) * (p-c));
  }

  /**
   * Liefert eine Repraesentation dieses Shape in Form eines Strings.
   * Alle nach aussen sichtbaren Attribute werden zusammen mit ihren Werten
   * aufgelistet.
   *
   * @return eine textuelle Repraesentation dieses Shape
   */
  public String toString() {
    // Gebt ein String zurueck.
    return "(" + x + ", " + y + ", " + z + "), die Farbe: " + colour;
  }
}
