/******************************  Sphere.java  *********************************/
import java.lang.Math;
/**
 * Diese Klasse repraesentiert eine Kugel im dreidimensionalen Raum. Die Kugel
 * wird durch ihren Mittelpunkt und ihren Radius repraesentiert.
 */
public class Sphere extends Shape {

  /**
   * Radius der Kugel
   */
  private double radius;

  /**
   * Instanziiert eine neue Einheitskugel mit der Farbe "schwarz" und
   * dem Koordinatenursprung als Position.
   */
  public Sphere() {
    // 'this' Stichtwort wird benutzt um die andere 'Sphere' Konstruktor zu aufrufen.
    this("schwarz", 0, 0, 0, 0);
  }

  /**
   * Instanziiert eine neue Kugel mit dem uebergebenen Radius, der uebergebenen
   * Farbe colour und der Position
   * (x, y, z).
   *
   * @param radius
   *          der Radius der neuen Kugel
   * @param colour
   *          die Farbe der neuen Kugel
   * @param x
   *          die x-Koordinate der neuen Kugel
   * @param y
   *          die y-Kooridnate der neuen Kugel
   * @param z
   *          die z-Kooridnate der neuen Kugel
   *
   * @throws RuntimeException
   *         im Falle eines negativen Radius
   */
  public Sphere(String color, double x, double y, double z, double radius) {
    // Diesmal musste ich nicht 'this.colour' benutzen(aber konnte ich das machen), 
    // weil 'Shape' Klasse Attribut 'colour' heißt, aber der Parameter des Sphere-Methode heißt 'color'.
    // Diese Attribute gehören zu 'Shape' Superklasse. 
    colour = color;
    this.x = x;
    this.y = y;
    this.z = z;
    
    // Der Radiusattribut gehört zu Sphere Subklasse. Wir setzen iht durch die 'setRadius' Methode.
    setRadius(radius);
  }

  /**
   * Setzt den Radius dieser Kugel auf den uebergebenen Wert.
   *
   * @param radius
   *          der neue Radius dieses Kreises.
   *
   * @throws RuntimeException
   *          im Falle eines negativen Radius
   */
  public void setRadius(double radius) {
    // RuntimeException wenn Radius kleiner als 0 ist.
    if(radius < 0) {
		throw new RuntimeException("Radius kann nicht kleiner als 0 sein.");
    } 
    this.radius = radius;
  }

  /**
   * Liefert den Radius dieser Kugel.
   *
   * @return Radius dieser Kugel
   */
  public double getRadius() {
    return this.radius;
  }

  /**
   * Liefert den Abstand dieser Kugel zur uebergebenen Shape
   * other. Falls other auch eine Kugel ist, wird der
   * korrekte Abstand der beiden Kugeln geliefert, sonst das, was
   * other.getDistanceTo() liefert.
   *
   * @param other
   *          Der Shape zu der der Abstand von dieser Kugel berechnet
   *          werden soll.
   *
   * @return Abstand zu other
   */
  public double getDistanceTo(Shape other) {
    
    double m = 0;
    if (other instanceof Sphere) {
    	m = this.radius + ((Sphere)other).getRadius();
    }
    
    // Die Methode aus Superklasse wird aufgeruft.
    return super.getDistanceTo(other) - m;
  }

  /**
   * Berechnet das Volumen dieser Kugel.
   *
   * @return Volumen der Kugel
   */
  public double getVolume() {
  // In diese fall brauchen wir 'this' nicht. 
    return 4 * (Math.PI * radius*radius*radius) / 3;
  }

  /**
   * Berechnet den Flaecheninhalt dieser Kugel.
   *
   * @return Flaecheninhalt der Kugel
   */
  public double getArea() {
    return 4 * Math.PI * this.radius * radius;
  }

  /**
   * Liefert eine Repraesentation dieser Kugel in Form eines Strings.
   *
   * @return Repraesentation der Kugel
   */
  public String toString() {
    return "(" + x + ", " + y + ", " + z + ", radius: " + radius + "), die Farbe: " + colour;
  }
}
