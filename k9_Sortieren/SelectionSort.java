// SelectionSort
// Time complexity: O(n^2); Platzbedarf: O(n), zusätzlich zu den Daten O(1)
// In eine for Schleife lauf jeder Element wird mit andere Elemente vergleicht, die den Index i+1 haben.
// http://en.wikipedia.org/wiki/Selection_sort

import AlgoTools.IO;

public class SelectionSort {

	private static int[] unsorted = new int[]{5, 34, 2, 9, 1, 6, 9, 3, 2, 21, 28};
	
	public static void main(String[] args) {
		print_r(unsorted);
		sort();
		print_r(unsorted);
	}
	
	private static void sort() {
		int i, j, pos, min;
		
		for(i = 0; i < unsorted.length-1; i++) {
			min = unsorted[i];
			pos = i;
			for(j = i+1; j < unsorted.length; j++) {
				if(unsorted[j] < min) {
					min = unsorted[j];
					pos = j;
				}
			}
			unsorted[pos] = unsorted[i];
			unsorted[i] = min;
		}
	}
	
	private static void print_r(int[] arr) {
	System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+", ");
		}
	System.out.println("]");
	}

}