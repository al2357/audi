// MergeSort
import AlgoTools.IO;

public class MergeSort {

	private static int[] unsorted = new int[]{5, 34, 2, 9, 1, 6, 9, 3, 2, 21, 28};
	
	public static void main(String[] args) {
		print_r(unsorted);
		print_r(sort(unsorted));
	}
	
	private static int[] sort(int[] arr) {
		if(arr.length == 1) { return arr; }
		
		int halbe = arr.length/2;
		int[] halbe1 = new int[halbe];
		int[] halbe2 = new int[arr.length - halbe];
		
		int i;
		for(i=0; i<halbe; i++) 				{ halbe1[i] = arr[i]; }
		for(i=0; i<arr.length-halbe; i++) 	{ halbe2[i] = arr[halbe+i]; }
		
		halbe1 = sort(halbe1);
		halbe2 = sort(halbe2);
		
		return merge(halbe1, halbe2);
	}
	
	private static int[] merge(int[] h1, int[] h2) {
		int[] sortiert = new int[h1.length + h2.length];
		int i=0, j=0, k=0;
		
		while((i<h1.length) && (j<h2.length)) {
			if(h1[i]<h2[j]) { sortiert[k++] = h1[i++]; } 
			else 			{ sortiert[k++] = h2[j++]; }
		}
		
		while(i<h1.length) { sortiert[k++] = h1[i++]; };
		while(j<h2.length) { sortiert[k++] = h2[j++]; };
	
		return sortiert;
	}
	
	private static void print_r(int[] arr) {
	System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+", ");
		}
	System.out.println("]");
	}

}