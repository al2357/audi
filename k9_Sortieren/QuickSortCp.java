// SelectionSort
// Time complexity: O(n^2); Platzbedarf: O(n), zusätzlich zu den Daten O(1)
// In eine for Schleife lauf jeder Element wird mit andere Elemente vergleicht, die den Index i+1 haben.
// http://en.wikipedia.org/wiki/Selection_sort

import AlgoTools.IO;

public class QuickSortCp {

	private static int[] unsorted = new int[]{5, 34, 2, 9, 1, 6, 9, 3, 2, 21, 28};
	
	public static void main(String[] args) {
		print_r(unsorted);
		sort(unsorted, 0, unsorted.length-1);
		print_r(unsorted);
	}
	
	private static void sort(int[] arr, int start, int ende) {
		int i=start, j=ende;
		
		int mittel = (start + ende)/2;
		int pivot = arr[mittel];
		
		
if (arr[start] < arr[mittel]){
    if (arr[mittel] < arr[ende]) {
        pivot = arr[mittel];
    } else if (arr[ende] > arr[start]) {
        pivot = arr[ende];
    } else {
        pivot = arr[start];
    }
} else {
    if (arr[start] < arr[ende]) {
        pivot = arr[start];
    } else if(arr[mittel] > arr[ende]) {
        pivot = arr[mittel];
    } else {
        pivot = arr[ende];
    }

}
    
    IO.println("start: "+arr[start]+"; end: "+arr[ende]+"; middle: "+arr[mittel]+"; pivot: "+pivot);
		
		do {
			while (arr[i] < pivot) i++;
			while (arr[j] > pivot) j--;
			if(i <= j) { 
				swap(arr, i, j); 
				i++;
				j--;
			}
				
		} while (i <= j);
		
		if(start < j) 	{ sort(arr, start, j); }
		if(ende > i) 	{ sort(arr, i, ende); }
	}
	
	private static void print_r(int[] arr) {
	System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+", ");
		}
	System.out.println("]");
	}
	
	private static void swap(int[] arr, int a, int b) {
		int temp;
		temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}

}
