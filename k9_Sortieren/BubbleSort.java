// BubbleSort
// Timekomplexitat: O(n^2)
// Nur angrenzende Elemente(i und i+1) werden vergleicht(for Schleife). Wenn alle in ordnung sind, terminiert while Schleife.
// http://en.wikipedia.org/wiki/Bubble_sort


import AlgoTools.IO;

public class BubbleSort {

	private static int[] unsort = new int[]{5, 34, 2, 9, 1, 6, 9, 3, 2, 21, 28};
	
	public static void main(String[] args) {
		print_r(unsort);
		sort();
		print_r(unsort);
	}
	
	private static void sort() {
		int temp;
		boolean getauscht;
		do {
			getauscht = false;
			
			for(int i = 0; i < unsort.length-1; i++) {
				if(unsort[i] > unsort[i+1]) {
					temp = unsort[i];
					unsort[i] = unsort[i+1];
					unsort[i+1] = temp;
					getauscht = true;
				}
			}
			
		} while(getauscht);
	}
	
	private static void print_r(int[] arr) {
	System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+", ");
		}
	System.out.println("]");
	}

}