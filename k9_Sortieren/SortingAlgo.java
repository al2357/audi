// SortingAlgorithms

import AlgoTools.IO;

public class SortingAlgo {

	private static int[] unsort = new int[]{5, 34, 2, 9, 1, 6, 9, 3, 2, 21, 28};
	
	public static void main(String[] args) {
		print_r(unsort);
		sort();
		print_r(unsort);
	}
	
	// BubbleSort
	private static void BubbleSort() {
		int temp;
		boolean getauscht;
		do {
			getauscht = false;
			for(int i = 0; i < unsort.length-1; i++) {
				if(unsort[i] > unsort[i+1]) {
					swap(i, i+1);
					getauscht = true;
				}
			}
		} while(getauscht);
	}
	
	// SelectionSort
	private static void SelectionSort() {
	int i, j, pos, min;
		for(i = 0; i < unsort.length-1; i++) {
			min = unsort[i];
			pos = i;
			for(j = i+1; j < unsort.length; j++) {
				if(unsort[j] < min) {
					min = unsort[j];
					pos = j;
				}
			}
			swap(pos, i);
		}
	}
	
	// MergeSort
	private static int[] sort(int[] arr) {
		if(arr.length == 1) { return arr; }
		
		int halbe = arr.length/2;
		int[] halbe1 = new int[halbe];
		int[] halbe2 = new int[arr.length - halbe];
		
		int i;
		for(i=0; i<halbe; i++) 				{ halbe1[i] = arr[i]; }
		for(i=0; i<arr.length-halbe; i++) 	{ halbe2[i] = arr[halbe+i]; }
		
		halbe1 = sort(halbe1);
		halbe2 = sort(halbe2);
		
		return merge(halbe1, halbe2);
	}
	
	private static int[] merge(int[] h1, int[] h2) {
		int[] sortiert = new int[h1.length + h2.length];
		int i=0, j=0, k=0;
		
		while((i<h1.length) && (j<h2.length)) {
			if(h1[i]<h2[j]) { sortiert[k++] = h1[i++]; } 
			else 			{ sortiert[k++] = h2[j++]; }
		}
		
		while(i<h1.length) { sortiert[k++] = h1[i++]; };
		while(j<h2.length) { sortiert[k++] = h2[j++]; };
	
		return sortiert;
	}
	
// Zusätzlichen Methoden
	private static void print_r(int[] arr) {
	System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+", ");
		}
	System.out.println("]");
	}
	
	private static void swap(int a, int b) {
		int temp;
		temp = unsort[a];
		unsort[a] = unsort[b];
		unsort[b] = temp;
	}

}