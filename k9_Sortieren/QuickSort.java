// SelectionSort
// Time complexity: O(n^2); Platzbedarf: O(n), zusätzlich zu den Daten O(1)
// In eine for Schleife lauf jeder Element wird mit andere Elemente vergleicht, die den Index i+1 haben.
// http://en.wikipedia.org/wiki/Selection_sort

import AlgoTools.IO;

public class QuickSort {

	private static int[] unsorted = new int[]{8, 5, 2, 9, 6, 3, 7, 4};
	
	public static void main(String[] args) {
		print_r(unsorted);
		sort(unsorted, 0, unsorted.length-1);
		print_r(unsorted);
	}
	
	private static void sort(int[] arr, int start, int ende) {
		int i=start, j=ende;
		
		//int mittel = ;
		int pivot = wahlePivotelement(arr, start, ende);
		IO.println("----------");
		IO.println("pivot: "+pivot);
		
		do {
			while (arr[i] < pivot) i++;
			while (arr[j] > pivot) j--;
			if(i <= j) { 
				swap(arr, i, j); 
				i++;
				j--;
			}
				
		} while (i <= j);
		
		IO.println("start: "+start+"; ende: "+ende+"; i: "+i+"; j: "+j+";");
		print_r(arr);
		if(start < j) 	{ sort(arr, start, j); }
		if(ende > i) 	{ sort(arr, i, ende); }
	}
	
	private static int wahlePivotelement(int[] arr, int erste, int letzte) {
		int mitte = arr[(erste+letzte)/2];
		int start = arr[erste];
		int ende = arr[letzte];
		
		boolean a = start < mitte;
		boolean b = start < ende;
		
			if(a != b) { return start; }
		
		boolean c = mitte < ende;
		
			return (a == c) ? mitte : ende;
		
	}
	
	private static void print_r(int[] arr) {
	System.out.print("[");
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i]+", ");
		}
	System.out.println("]");
	}
	
	private static void swap(int[] arr, int a, int b) {
		int temp;
		temp = arr[a];
		arr[a] = arr[b];
		arr[b] = temp;
	}

}