import AlgoTools.IO;

public class WurzelZwei {
	// Variablen von Rekursion Methode wird hier definiert.
	private static double wurzel_zwei;
	private static double nth_wurzel;
	
	public static void main(String[] args) {
		int n;
		// Die Genaugkeitszahl wird eingelesen.
		do {
			n = IO.readInt("Eine Zahl grossere als 0 bitte: ");
		} while(n < 1);
		
		// Wir rufen die wurzelRechne Methode auf.
		double ergebnis = wurzelRechne(n);
		IO.println(ergebnis);
	}
	
	private static double wurzelRechne(int n) {
		// Der Basisfall ist wenn n == 0.
		if (n == 0) {return 1;}
		
		// Wenn n nicht gleich 0 ist, rufen wir rekursiv die Methode 'wurzelRechne' auf.
		nth_wurzel = wurzelRechne(n-1);
		
		// Mit dem 'n-1' Ergebnis rechnen wir die 'nth' Wurzel.
		wurzel_zwei = 0.5 * (nth_wurzel + 2.0/nth_wurzel);
		return wurzel_zwei;	 
	}
}
