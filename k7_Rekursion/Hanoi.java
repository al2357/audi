import AlgoTools.IO;

public class Hanoi {
	public static void main(String[] args) {
		int scheibe = IO.readInt("Geben Sie die Nummer von Teller ein: ");
		verlege(scheibe, 'A', 'B', 'C');
	}
	
	private static void verlege(int n, char start, char zwischen, char ziel) {
		if(n > 0) {
			verlege(n-1, start, ziel, zwischen);
			IO.println("Verlege Teller "+n+" von "+start+" nach "+ziel+".");
			verlege(n-1, zwischen, start, ziel);
		}
	}
}
