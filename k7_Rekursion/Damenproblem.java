import AlgoTools.IO;

public class Damenproblem {
  private static int aufrufe = 0;
  private static boolean loeseDamenProblem(boolean[][] brett, int fehlendeDamen) {
	aufrufe++;
	// Ein grundlegender Fall wenn alle Damen sind gelegt.
	if(fehlendeDamen < 0) {return true;}

	// Fuer jede Ziele(fehlendeDamen) versuchen wir eine Spalte(i).
	for(int i = 0; i < brett[fehlendeDamen].length; i++) {
	
		// Wenn [fehlendeDamen][i] feld 'Okay' und falsch ist, macht es 'true' und gib 'true' zurueck...
		if(feldOkay(brett, fehlendeDamen, i) && !brett[fehlendeDamen][i]) {
			brett[fehlendeDamen][i] = true;
			// Die Rekursion - gehen wir zur naechste Zeile.
			loeseDamenProblem(brett, fehlendeDamen-1);
			return true;
		// Wenn [fehlendeDamen][i] feld schon 'true' ist, muss es auch falsch sein.
		} else if (brett[fehlendeDamen][i]) {
			brett[fehlendeDamen][i] = false;
		}
	}
	// Die Rekursion - gehen wir zur frueher Zeile.
	loeseDamenProblem(brett, fehlendeDamen+1);

    return false;
  }

  private static boolean feldOkay(boolean[][] brett, int zeile, int spalte) {
    // teste ob eine Dame in derselben Spalte oder Zeile gibt
    for (int i = 0; i < brett.length; i++) {
      if (brett[zeile][i] || brett[i][spalte]) {
        return false;
      }
    }
    //teste Diagonale nach rechts unten
    for (int z = zeile + 1, s = spalte + 1; z < brett.length && s < brett.length; 
         z++, s++) {

      if (brett[z][s])
        return false;

    }
    //teste Diagonale nach links oben
    for (int z = zeile - 1, s = spalte - 1; z >= 0 && s >= 0; 
         z--, s--) {

      if (brett[z][s])
        return false;

    }
    //teste Diagonale nach links unten
    for (int z = zeile + 1, s = spalte - 1; z < brett.length && s >= 0; 
        z++, s--) {

      if (brett[z][s])
        return false;

    }
    //teste Diagonale nach rechts oben
    for (int z = zeile - 1, s = spalte + 1; z >= 0 && s < brett.length; 
        z--, s++) {

      if (brett[z][s])
        return false;
    }
    //wenn nirgendwo in der Zeile, Spalte oder den Diagonalen eine Dame 
    //gefunden, ist das Feld gueltig.
    return true;
  }

  public static void gibErgebnisAus(boolean[][] brett) {

    //Ausgabe als Schachbrett-Visualisierung
    IO.print("   ");
    for (int j = 0; j < brett.length; j++) {
      IO.print(((char) ('a' + j)) + " ");
    }

    IO.println();

    IO.print("  ");
    for (int j = 0; j < brett.length; j++) {
      IO.print("+");
      IO.print("-");
    }

    IO.println("+");

    for (int i = 0; i < brett.length; i++) {
      IO.print((8 - i) + " ");
      for (int j = 0; j < brett[i].length; j++) {
        IO.print("|");
        IO.print(brett[i][j] ? "D" : " ");
      }
      IO.println("|");
      IO.print("  ");
      for (int j = 0; j < brett[i].length; j++) {
        IO.print("+");
        IO.print("-");
      }
      IO.println("+");
    }

    IO.print("\n\n");

    //Ausgabe als Liste
    int ausgaben = 0;
    for(int zeile = 0; zeile < brett.length; zeile++) {
      for(int spalte = 0; spalte < brett[zeile].length; spalte++) {

        if(brett[zeile][spalte]) {
          IO.print(((char) ('a' + spalte)) + "" + (8 - zeile));
          ausgaben++;

          //Komma beim letzten Element auslassen
          if(ausgaben < brett.length) {
            IO.print(",");
          }
        }

      }
    }

    IO.println(); //Zeilumbruch am Ende

  }


  public static void main(String[] args) {

    //Initialisiere und deklariere Spielfeld
    boolean[][] brett = new boolean[8][8];
    
    //versuche Damenproblem zu lösen
    if (loeseDamenProblem(brett, 7)) {
      gibErgebnisAus(brett);
    }
    else {
      IO.println("Eine Lösung konnte nicht gefunden werden. Das Damenproblem für die Kantelänge 8 ist aber lösbar, was bedeutet, dass Ihr Algorithmus noch nicht richtig funktioniert.");
    }
    IO.println("Aufrufe: "+aufrufe);
  }
}

