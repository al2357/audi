import AlgoTools.IO;

public class Methoden {

  public static double vectorLength(double[] vector) {
  	double derBetrag = 0;
  	
  	// Eine Schleife wird benutzt um die Elemente des Vektors zu potenzieren und addieren.
  	for(int i = 0; i<vector.length; i++) {
  		derBetrag += vector[i]*vector[i];
  	}
  	
  	// Die Summe wird radiziert.
  	double dasErgebnis = Math.sqrt(derBetrag);
  	
  	// Das Ergebnis wird zu drei Stellen hinder dem Comma reduziert und zurueckgegeben.
    return Math.round(dasErgebnis*1000)/1000.0;
  }
  
    
/** 
/* Diese zusaetzliche Methode hilft die 'DRYness' von diese Klasse zu behalten.
/* http://en.wikipedia.org/wiki/Don%27t_repeat_yourself
/* Es kann fuer beide Operationen, die modulo und normale Abteilung, benutzt werden.
/* Es nimmt den @a Dividend und den @b Divisor an.
/* @param - a             ein integer Dividend  
/* @param - b             ein integer Divisor
/* @param - dieOperation  die Operationsanzeige, 'm' steht fuer (m)odulo und 'd' fuer
/*   normale (d)ivision
/* @return				  das Ergebnis der modulo oder normale Divsion
*/
  private static int abteilungSchleife(int a, int b, char dieOperation) {
  	int temp = b;
  	int dasErgebnis = 0;
  	for(temp = b; temp <= a; temp = temp + b) {
  		dasErgebnis++;
  	};	
  	return (dieOperation == 'd') ? dasErgebnis : Math.abs(a-(temp-b)); 
  }

  public static int modulo(int a, int b) {
	  if(a <= 0 || b <= 0) {
	  	throw new RuntimeException("b ist Null");
	  }
  
    // die Methode fuer module Abteilung
    return abteilungSchleife(a, b, 'm');
  }

  public static int div(int a, int b) {
	  if(a <= 0 || b <= 0) {
	  	throw new RuntimeException("b ist Null");
	  }
	  
    // die Methode fuer standard Abteilung
  	return abteilungSchleife(a, b, 'd');
  }

  public static void main(String[] args) {

    char auswahl;
    do {
      IO.println("Bitte wählen Sie die zu testende Methode durch Eingabe des zugehörigen Buchstaben aus.");
      auswahl = IO.readChar("(v)ectorLength, (m)odulo, (d)ivison, (e)xit: ");

      int a,b;
      double[] vector;
      switch(auswahl) {
        case 'v': //Vektor-Länge
          vector = IO.readDoubles("Bitte geben Sie die Vektorelemente leerzeichengetrennt ein: ");
          IO.println("vectorLength: " + vectorLength(vector));
          break;
        case 'm': //Modulo
          do {
            a = IO.readInt("Bitte Wert 1 > 0: ");
            b = IO.readInt("Bitte Wert 2 > 0: ");
          } while( a <= 0 || b <= 0);
          IO.println("modulo: " + modulo(a,b));
          break;
        case 'd': //Division
          do {
            a = IO.readInt("Bitte Wert 1 > 0: ");
            b = IO.readInt("Bitte Wert 2 > 0: ");
          } while( a <= 0 || b <= 0);
          IO.println("div: " + div(a,b));
          break;
        case 'e': //Nichts zu tun...
          break;
        default:
          IO.println("Ungültige Eingabe.");
        break;
      }

    } while(auswahl != 'e');

  }

}
