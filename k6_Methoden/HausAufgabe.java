import AlgoTools.IO;

public class HausAufgabe {
    public static double vectorLength(double[] vector) {

        double sum = 0;

        for(int i = 0; i < vector.length; i++) {
            sum += vector[i] * vector[i];
        }

        return Math.sqrt(sum);
    }

    public static int modulo(int a, int b) {

        if(a < 1 || b < 1) {
            throw new RuntimeException("a oder b sind kleiner als 1");
        }

        do {
            a = a - b;
        } while(a >= b);

        return a;
    }

    public static int div(int a, int b) {

        if(a < 1 || b < 1) {
            throw new RuntimeException("a oder b sind kleiner als 1");
        }

        int counter = 0;

        do {
            a = a - b;
            counter += 1;
        } while(a >= b);

        return counter;
    }

    public static void main(String[] args) {

        char auswahl;
        do {
            IO.println("Bitte wählen Sie die zu testende Methode durch Eingabe des zugehörigen Buchstaben aus.");
            auswahl = IO.readChar("(v)ectorLength, (m)odulo, (d)ivison, (e)xit: ");

            int a,b;
            double[] vector;
            switch(auswahl) {
                case 'v': //Vektor-Länge
                    vector = IO.readDoubles("Bitte geben Sie die Vektorelemente leerzeichengetrennt ein: ");
                    IO.println("vectorLength: " + vectorLength(vector));
                    break;
                case 'm': //Modulo
                    do {
                        a = IO.readInt("Bitte Wert 1 > 0: ");
                        b = IO.readInt("Bitte Wert 2 > 0: ");
                    } while( a <= 0 || b <= 0);
                    IO.println("modulo: " + modulo(a,b));
                    break;
                case 'd': //Division
                    do {
                        a = IO.readInt("Bitte Wert 1 > 0: ");
                        b = IO.readInt("Bitte Wert 2 > 0: ");
                    } while( a <= 0 || b <= 0);
                    IO.println("div: " + div(a,b));
                    break;
                case 'e': //Nichts zu tun...
                    break;
                default:
                    IO.println("Ungültige Eingabe.");
                    break;
            }

        } while(auswahl != 'e');

    }
}
